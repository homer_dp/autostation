'use strict';
angular.module("Races").factory("racesService", racesService);

racesService.$inject = ["$http", "constantsService"];

function racesService($http, constantsService) {

   var doRequestRaces = function(strDate1, strDate2) {
       return $http.get(constantsService.racesUrl.races,
              { params: {
                          date1: strDate1,
                          date2: strDate2
                         }
              });
    }

   var doRequestRace = function(hDate, tripId) {
       return $http.get(constantsService.racesUrl.race,
              { params: {
                          date: hDate,
                          vr: tripId
                         }
              });
    }

   var doRequestVedomXls = function(hDate, tripId, doClose, doSend) {
       return $http.get(constantsService.racesUrl.vedomXls,
              { params: {
                          date: hDate,
                          tripid: tripId,
                          close: doClose?1:0,
                          sendmail: doSend?1:0
                         },
                responseType: 'arraybuffer'
              });
    }

   var doRequestRaceCtl = function(command, hDate, tripId, doClose, reason) {
       return $http.get(constantsService.racesUrl.racectl,
              { params: {
                          date: hDate,
                          tripid: tripId,
                          command: command,
                          close: doClose,
                          reason: reason
                         }
              });
    }

    return {
         getRaces: function(strDate1, strDate2) {
            return doRequestRaces(strDate1, strDate2);
         },
         getRace: function(hDate, tripId) {
            return doRequestRace(hDate, tripId);
         },
         getVedomXls: function(hDate, tripId, doClose, doSend) {
            return doRequestVedomXls(hDate, tripId, doClose, doSend);
         },
         getRaceCtl: function(command, hDate, tripId, doClose, reason) {
            return doRequestRaceCtl(command, hDate, tripId, doClose, reason);
         }
    };


};

