"use strict";

angular.module("Races", ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.bootstrap'])
  .controller("ModalControllerCommand", ModalControllerCommand)
  .controller("RacesController", RacesController)

RacesController.$inject = ["$rootScope", "$scope", "$window", "racesService", "messageService", "stateService", "constantsService", "i18nService", "$http", "$log", "$location", "$filter", "$uibModal", "uiGridConstants" ];
ModalControllerCommand.$inject = ["$scope", "$uibModal", "$uibModalInstance", "$log", "racesService", "param"];

function RacesController($rootScope, $scope, $window, racesService, messageService, stateService, constantsService,  i18nService, $http, $log, $location, $filter, $uibModal, uiGridConstants ) {
  var vm = this;
  i18nService.setCurrentLang('ru');
  vm.paginationOptions = {
    pageNumber: 1,
    pageSize: 20,
    sort: null
  };
  vm.filterOptions = {};

  vm.eDate= new Date();
  vm.bDate= new Date();
  vm.bDate.setDate(vm.bDate.getDate()-1);
  vm.datepickers = [];
  vm.datepickers[0]=false;
  vm.datepickers[1]=false;
  vm.loading=false;
  vm.gridSelected=false;
  vm.entitySelected={};
  vm.styleSelected="success";
  vm.gridMessage="";

  vm.open = function(which) {
	vm.datepickers[which] = true;
  };

  vm.dateOptions = {
  	startingDay: 1
  };

 
  vm.statusStyleMain = function(grid, row, col, rowRenderIndex, colRenderIndex) {
       var status = row.entity.stateTripStyle;
       if (status == "warning") {
           return 'raceStatusWarning';
       };
       if (status == "error") {
           return 'raceStatusError';
       };
       if (status == "primary") {
           return 'raceStatusPrimary';
       };
       if (status == "success") {
           return 'raceStatusSuccess';
       };
  };

  vm.canBlock = function() {
      var entity = vm.entitySelected;
      if (entity) {
        if (("$$hashKey" in entity) && (entity.isBlocked !== 1)) {
          return true;
        } 
      }
      return false;
  };

  vm.canCancel = function() {
      var entity = vm.entitySelected;
      if (entity) {
        if (("$$hashKey" in entity) && (entity.isCanceled !== 1)) {
          return true;
        } 
      }
      return false;
  };

  vm.canCloseVed = function() {
      var entity = vm.entitySelected;
      if (entity) {
        if (("$$hashKey" in entity) && (entity.vedomId === "")) {
          return true;
        } 
      }
      return false;
  };

  vm.getVedHref = function() {
      var entity = vm.entitySelected;
      if (entity && ("$$hashKey" in entity)) {
         return constantsService.racesUrl.vedomXls+'?token='+$rootScope.userGlobal.token+'&tripid='+entity.tripId+'&date='+entity.hDate+'&close=0&sendmail=0';
      }
      else {
         return "";
      };
  };

  vm.getVed = function() {
      var entity = vm.entitySelected;
      if (entity && ("$$hashKey" in entity)) {
         vm.loading=true;
         racesService.getVedomXls(entity.hDate, entity.tripId, 0, 0)
         .success(function (response, status, header, config) {
           vm.loading=false;
           var fileName = "vedom_"+entity.hDate+"_"+entity.tripId+".xlsx";
           var saveData = (function () {
               var a = document.createElement("a");
               document.body.appendChild(a);
               a.style = "display: none";
               return function (data, fileName) {
                   var blob = new Blob([data], {type: "application/octet-stream"});
                   var url_ = window.webkitURL || window.URL;
                   var url = url_.createObjectURL(blob);
                   a.href = url;
                   a.download = fileName;
                   a.click();
                   url_.revokeObjectURL(url);
               };
           }());
           saveData(response, fileName);
         }).error(function(response, status) {
	   vm.loading=false;
              if ((status == -1)||(!response)) {
                  messageService.error("Немає зв\'язку з сервером", "race", "");
              } else {
                  var dataView = new DataView(response);
                  var decoder = new TextDecoder("utf-8");
                  var decodedString = decoder.decode(dataView);
                  response = JSON.parse(decodedString);
                  messageService.error(response.error.name, response.error.type, response.error.param);
              }
         });
       };
  };


  vm.gridOptions = {
    enableRowSelection: true,
    enableFullRowSelection: true,
    enableSelectAll: false,
    noUnselect: true,
    multiSelect: false,
    enableRowHeaderSelection: true,
    selectionRowHeaderWidth: 25,
    rowHeight: 25,
    paginationPageSizes: [10, 15, 20, 25, 30, 35, 40, 50, 100],
    paginationPageSize: 20,
    useExternalPagination: false,
    useExternalSorting: false,
    enableSorting: false,
    enableFiltering: true,
    useExternalFiltering: false,
    expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:{{(row.entity.subGridOptions.rowHeight+1) * row.entity.threadCount + row.grid.headerHeight + 12}}px;"></div>',
    expandableRowHeight: 270,
    expandableRowScope: {
      controller: vm
    },
    onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        vm.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            vm.gridSelected=true;
            vm.entitySelected=row.entity;
        });
        vm.gridApi.expandable.on.rowExpandedStateChanged($scope,function(row){
            vm.getSubPage(row.entity.hDate, row.entity.tripId, row);
           var height = (row.entity.subGridOptions.rowHeight+1) * row.entity.threadCount + row.grid.headerHeight + 10;
           vm.gridOptions.expandableRowHeight = height;
           row.expandedRowHeight = height;
        });
  }};
 
  vm.gridOptions.columnDefs = [
        { name: 'depDate', displayName: 'Дата', width: '6%', enableFiltering: false },
        { name: 'depTime', displayName: 'Час відпр.', width: '6%', enableFiltering: false },
        { name: 'tripNum', displayName: '№ рейса', width: '6%'},
        { name: 'stFormName', displayName: 'Формування', width: '12%' },
        { name: 'stDestName', displayName: 'Призначення', width: '13%' },
        { name: 'placesFree', displayName: 'Вільно місць', width: '5%', enableFiltering: false },
        { name: 'placesBusy', displayName: 'Зайнято місць', width: '5%', enableFiltering: false  },
        { name: 'stateTrip', displayName: 'Статус', width: '7%', cellClass: vm.statusStyleMain,
          filter: {
              type: uiGridConstants.filter.SELECT,
              selectOptions: [ { value: 'Блоковано', label: 'Блоковано' }, 
                               { value: 'Вiдомiсть', label: 'Відомість' },
                               { value: 'Зiрвано', label: 'Зірвано' },
                               { value: 'Прибув', label: 'Прибув' } ],
              disableCancelFilterButton: true
          }
        },
        { name: 'carrierName', displayName: 'Перевізник', width: '15%' },
        { name: 'busName', displayName: 'Автобус', width: '10%', enableFiltering: false },
        { name: 'vedomNum', displayName: '№ Відомості', width: '10%', enableFiltering: false }
  ];

  vm.hideError = function() {
     delete $rootScope.message;
  };

  vm.searchClick = function() {
     vm.getPage();
  };

  vm.openCommandDlg = function (command) {
    if ((command!=="vedom") && (command!=="block") && (command!=="cancel")) return;
    var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        templateUrl: 'views/races/commandDialogTemplate.html',
        controller: 'ModalControllerCommand',
        controllerAs: 'commandCtrl',
        resolve: {
           param: function () {
               return {
                 'entitySelected': vm.entitySelected,
                 'command':        command
               };
           }
         }
      });
      modalInstance.result.then(function (result) {
        if (result && (result!==undefined)) {
          if ('isCanceled' in result) {vm.entitySelected.isCanceled=result.isCanceled};
          if ('isBlocked' in result) {vm.entitySelected.isBlocked=result.isBlocked};
          if ('vedomId' in result) {vm.entitySelected.vedomId=result.vedomId};
          if ('vedomNum' in result) {vm.entitySelected.vedomNum=result.vedomNum};
          if ('stateTrip' in result) {vm.entitySelected.stateTrip=result.stateTrip};
          if ('stateTripStyle' in result) {vm.entitySelected.stateTripStyle=result.stateTripStyle};
          vm.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ALL );
        };
      }, function () {
        //vm.getPage();
      });
  };

  vm.getPage = function() {
      vm.loading=true;
      var strDate1 = $filter('date')(vm.bDate, "yyyy-MM-dd");
      var strDate2 = $filter('date')(vm.eDate, "yyyy-MM-dd");
      racesService.getRaces(strDate1, strDate2)
      .success(function (response) {
        vm.loading=false;
        if (response.error) {
           vm.gridOptions.data = [];
           if (response.error.code !== 'E_NODATA') {
             messageService.error(response.error.name, "races", response.error.param);
           }
        }
        else {
           for(var i = 0; i < response.length; i++){
             response[i].subGridOptions = {
               columnDefs: [ { name: 'stName', displayName: 'Станція', width: '25%'},
                             { name: 'distance', displayName: 'Відстань', width: '10%' },
                             { name: 'arrTime', displayName: 'Прибуття', width: '10%' },
                             { name: 'depTime', displayName: 'Відправлення', width: '10%' },
                             { name: 'passIn', displayName: 'Посадка (місць)', width: '12%' },
                             { name: 'passOut', displayName: 'Висадка (місць)', width: '12%' },
                             //{ name: 'embarkState', displayName: 'Властивість', width: '14%' },
                           ],
               data: [],
               rowHeight: 24
             }
           }
           vm.gridOptions.data = response;
           stateService.races.save(vm);
        }
      }).error(function(response, status) {
		vm.loading=false;
                vm.gridOptions.data = [];
                if ((status == -1)||(!response)) {
                    messageService.error("Немає зв\'язку з сервером", "races");
                } else {
                    messageService.error(response.error.name, response.error.type, response.error.param);
                }
		console.log(response);
      });
    };

    vm.getSubPage = function(hDate,tripId,row) {
      vm.loading=true;
      racesService.getRace(hDate, tripId)
      .success(function (response) {
        vm.loading=false;
        if (response.error) {
           if (response.error.code !== 'E_NODATA') {
             messageService.error(response.error.name, "race", response.error.param);
           }
           row.entity.subGridOptions.data = [];
        }
        else {
           row.entity.subGridOptions.data = response;
           row.entity.threadCount = response.length;  //просто обновим на всякий...
        }
      }).error(function(response, status) {
	   vm.loading=false;
           if ((status == -1)||(!response)) {
               messageService.error("Немає зв\'язку з сервером", "race", "");
           } else {
               messageService.error(response.error.name, response.error.type, response.error.param);
           }
           row.entity.subGridOptions.data = [];
      });
    };


    stateService.races.restore(vm);
    vm.getPage();
};


function ModalControllerCommand($scope, $uibModal, $uibModalInstance, $log, racesService, param) {
        var mcvm = this;
        if ((param.command!=="vedom") && (param.command!=="block") && (param.command!=="cancel")) return;
        mcvm.command = param.command;
        mcvm.entitySelected=param.entitySelected;
        mcvm.commandInProgress=false;
        mcvm.commandMessages=[];
        mcvm.reasonCode = undefined;
        mcvm.result = undefined;

        if ((mcvm.command==="vedom") && (mcvm.entitySelected.vedomId === "")) { mcvm.doClose = true; }
        if ((mcvm.command==="vedom") && (mcvm.entitySelected.vedomId !== "")) { mcvm.doClose = false; }
        if ((mcvm.command==="block") && (mcvm.entitySelected.isBlocked === 0)) { mcvm.doClose = true; }
        if ((mcvm.command==="block") && (mcvm.entitySelected.isBlocked !== 0)) { mcvm.doClose = false; }
        if ((mcvm.command==="cancel") && (mcvm.entitySelected.isCanceled === 0)) { mcvm.doClose = true; }
        if ((mcvm.command==="cancel") && (mcvm.entitySelected.isCanceled !== 0)) { mcvm.doClose = false; }

        if (mcvm.doClose && (mcvm.command==="vedom")) { 
            mcvm.commandTxt="Закрити відомість. Припинити продаж."; 
            mcvm.commandDesc="При закритті відомості буде припинено продаж та автоматично надіслано повідомлення перевізнику з електронною копією відомості (за наявністю відповідних настроювань)"; 
        };
        if (mcvm.doClose && (mcvm.command==="block")) { 
            mcvm.commandTxt="Заблокувати рейс"; 
            mcvm.commandDesc="Блокування рейсу призведе до неможливості здійснення продажу квитків"; 
        };
        if (mcvm.doClose && (mcvm.command==="cancel")) { 
            mcvm.commandTxt="Встановити зрив рейсу за вказаною причиною."; 
            mcvm.commandDesc="Зрив рейсу призведе до неможливості здійснення продажу квитків. Встановлюється в разі неможливості здійснення поїздки з раптово-виниклих причин технічного або форс-мажорного характеру"; 
        };
        if (!mcvm.doClose && (mcvm.command==="vedom")) { 
            mcvm.commandTxt="Зняти з відомості статус 'Закрита'. Відновити продаж."; 
            mcvm.commandDesc="Якщо зняти з відомості статус 'Закрита', буде відновлено можливість продажу квитків на даний рейс за наявністю вільних місць. Якщо настане час автоматичного закриття відомості, вона буде закрита автоматично без попередження."; 
        };
        if (!mcvm.doClose && (mcvm.command==="block")) { 
            mcvm.commandTxt="Розблокувати рейс"; 
            mcvm.commandDesc="Розблокування рейсу відкриє можливість здійснення продажу квитків на даний рейс за наявністю вільних місць"; 
        };
        if (!mcvm.doClose && (mcvm.command==="cancel")) { 
            mcvm.commandTxt="Зняти з рейсу статус 'Зірваний'"; 
            mcvm.commandDesc="Зняття з рейсу статусу 'Зірваний' в разі зникнення або усунення причин, що призвели до зриву рейсу. Буде відновлено можливість продажу квитків на даний рейс за наявністю вільних місць"; 
        };
  
        mcvm.ok = function() {
            mcvm.doCommand();
        };

        mcvm.cancel = function() {
            $uibModalInstance.close(mcvm.result);
        };

        mcvm.doCommand = function () {

            if ((mcvm.command==="cancel")&&(mcvm.entitySelected.isCanceled===0)&&(!mcvm.reasonCode)) {
                 mcvm.commandMessages.push({'status':false,'code':'E_NOREASON', 'name':'Не обрано причину зриву', 'param':''});
                 return;
            }
            
            mcvm.tripId = mcvm.entitySelected.tripId;
            mcvm.hDate = mcvm.entitySelected.hDate;
            mcvm.commandInProgress=true;
            mcvm.commandMessages=[];
            racesService.getRaceCtl(mcvm.command, mcvm.hDate, mcvm.tripId, mcvm.doClose?1:0, mcvm.reasonCode?mcvm.reasonCode:"")
            .success(function (response) {
              mcvm.commandInProgress=false;
              if (response.error) {
                 mcvm.commandMessages.push({'status':false,'code':response.error.code, 'name':response.error.name, 'param':response.error.param});
              }
              else {
                 mcvm.commandMessages.push({'status':true,'code':'УСПІШНО', 'name':'Операція виконана успішно!', 'param':'Рейс: '+mcvm.entitySelected.tripNum});
                 mcvm.result = response;
              }
            }).error(function(response, status) {
                      mcvm.commandInProgress=false;
                      if ((!response)||(response.status === -1)) {
                          mcvm.commandMessages.push({'status':false,'code':"NO_CONNECTION", 'name':'Немає зв\'язку з сервером', 'param':''});
                      } else {
                          mcvm.commandMessages.push({'status':false,'code':response.error.code, 'name':response.error.name, 'param':response.error.param});
                      }
            });
            
        };

};
