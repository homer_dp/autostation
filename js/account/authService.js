
'use strict';
angular.module("Account").factory("authService", authService);
authService.$inject = ["$http", "$location", "$cookies", "$rootScope", "messageService", "constantsService"];

function authService($http, $location, $cookies, $rootScope, messageService, constantsService) {
    var service = {};
    service.setCertificates = setCertificates;
    service.removeCertificates = removeCertificates;
    service.login = login;
    return service;

    function login(username, password, callback) {
        $http({
            url: constantsService.accountnUrl.login,
            method: "POST",
            data: "login="+username+"&"+"password="+password
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(response) {
            callback(response);
        });
    };

    function setCertificates(username, password, user) {
        $rootScope.userGlobal = {
            login: username,
            password: password,
            agent: user.agent,
            allowsale: user.allowsale,
            carriers: user.carriers,
            cashier: user.cashier,
            mode: user.mode,
            token: user.token
        }
       
        $http.defaults.headers.common.session = $rootScope.userGlobal.token;
        $http.defaults.headers.common.userlogin = $rootScope.userGlobal.login;
        $cookies.putObject("userAutostation", $rootScope.userGlobal);
        
    }
    function removeCertificates() {
        $rootScope.userGlobal = {};
        $cookies.remove("userAutostation");
        // How to destroy object instead setting of empty string?
        $http.defaults.headers.common.session = "";
        $http.defaults.headers.common.userlogin = "";
    }
};

