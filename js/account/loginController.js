'use strict';
angular.module("Account").controller("LoginController", LoginController);

LoginController.$inject = ["$scope", "authService", "messageService", "stateService", "$location"];
function LoginController($scope, authService, messageService, stateService, $location) {
    var vm = this;
    (function () {
        authService.removeCertificates();
    })();
    vm.login = function () {
        authService.login(vm.username, vm.password, function (response) {
            if (response.status === 200) {
                stateService.clean();
                authService.setCertificates(vm.username, vm.password, response.data);
                $location.path('/');
            } else {
                vm.loading = false;
                if (response.status === -1) {
                    messageService.error("Немає зв\'язку з сервером", "NO_CONNECTION", "");
                } else {
                    messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
                }
            }

            if (response.status === 401) {
                $location.path('/login');
                messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
            }

        });
    };



};
