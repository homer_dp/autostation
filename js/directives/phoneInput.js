﻿'use strict';

angular.module("Autostation").directive("phoneInput", function () {
    var oneNumbersCodeFlag, twoNumbersCodeFlag, threeNumbersCodeFlag, flag, compareValue;
    var countries = [
        { name: "Albania", code: "AL", pcode: "+355" },
        { name: "Armenia", code: "AM", pcode: "++374" },
        { name: "Austria", code: "AT", pcode: "+43" },
        { name: "Azerbaijan", code: "AZ", pcode: "+994" },
        { name: "Belarus", code: "BY", pcode: "+375" },
        { name: "Belgium", code: "BE", pcode: "+32" },
        { name: "Bosnia and Herzegovina", code: "BA", pcode: "+387" },
        { name: "Bulgaria", code: "BG", pcode: "+359" },
        { name: "Croatia", code: "HR", pcode: "+385" },
        { name: "Cyprus", code: "CY", pcode: "+357" },
        { name: "Czech Republic", code: "CZ", pcode: "+420" },
        { name: "Denmark", code: "DK", pcode: "+45" },
        { name: "Egypt", code: "EG", pcode: "+20" },
        { name: "Estonia", code: "EE", pcode: "+372" },
        { name: "Finland", code: "FI", pcode: "+358" },
        { name: "France", code: "FR", pcode: "+33" },
        { name: "Georgia", code: "GE", pcode: "+995" },
        { name: "Germany", code: "DE", pcode: "+49" },
        { name: "Greece", code: "GR", pcode: "+30" },
        { name: "Hungary", code: "HU", pcode: "+36" },
        { name: "Ireland", code: "IE", pcode: "+353" },
        { name: "Israel", code: "IL", pcode: "+972" },
        { name: "Italy", code: "IT", pcode: "+39" },
        { name: "Jordan", code: "JO", pcode: "+962" },
        { name: "Kazakhstan", code: "KZ", pcode: "+7" },
        { name: "Kosovo", code: "XK", pcode: "+383" },
        { name: "Kyrgyzstan", code: "KG", pcode: "+996" },
        { name: "Latvia", code: "LV", pcode: "+371" },
        { name: "Liechtenstein", code: "LI", pcode: "+423" },
        { name: "Lithuania", code: "LT", pcode: "+370" },
        { name: "Luxembourg", code: "LU", pcode: "+352" },
        { name: "Macedonia", code: "MK", pcode: "+389" },
        { name: "Moldova", code: "MD", pcode: "+373" },
        { name: "Monaco", code: "MC", pcode: "+377" },
        { name: "Netherlands", code: "NL", pcode: "+31" },
        { name: "Norway", code: "NO", pcode: "+47" },
        { name: "Palestine", code: "PS", pcode: "+970" },
        { name: "Poland", code: "PL", pcode: "+48" },
        { name: "Portugal", code: "PT", pcode: "+351" },
        { name: "Romania", code: "RO", pcode: "+40" },
        { name: "Russia", code: "RU", pcode: "+7" },
        { name: "San Marino", code: "SM", pcode: "+378" },
        { name: "Serbia", code: "RS", pcode: "+381" },
        { name: "Sierra Leone", code: "SL", pcode: "+232" },
        { name: "Slovakia", code: "SK", pcode: "+421" },
        { name: "Slovenia", code: "SI", pcode: "+386" },
        { name: "Somalia", code: "SO", pcode: "+252" },
        { name: "Spain", code: "ES", pcode: "+34" },
        { name: "Swaziland", code: "SZ", pcode: "+268" },
        { name: "Sweden", code: "SE", pcode: "+46" },
        { name: "Switzerland", code: "CH", pcode: "+41" },
        { name: "Syria", code: "SY", pcode: "+963" },
        { name: "Tajikistan", code: "TJ", pcode: "+992" },
        { name: "Tonga", code: "TO", pcode: "+676" },
        { name: "Turkey", code: "TR", pcode: "+90" },
        { name: "Turkmenistan", code: "TM", pcode: "+993" },
        { name: "Uganda", code: "UG", pcode: "+256" },
        { name: "Ukraine", code: "UA", pcode: "+38" },
        { name: "United Arab Emirates", pcode: "+AE", code: "971" },
        { name: "United Kingdom", code: "GB", pcode: "+44" },
        { name: "Uzbekistan", code: "UZ", pcode: "+998" },
        { name: "Vatican", code: "VA", pcode: "+379" }
    ];
    function removeLastNumber(valueArray, ngModelCtrl) {
        valueArray.pop();
        ngModelCtrl.$setViewValue(valueArray.join().replace(/,/g, ""));
        ngModelCtrl.$render();
    }
    function updateView(valueForView, ngModelCtrl) {
        ngModelCtrl.$setViewValue(valueForView.join().replace(/,/g, ""));
        ngModelCtrl.$render();
    }
    function oneNumbersCode(value, ngModelCtrl, count) {
        var valueArray = value.split('');
        if (valueArray.length === 3 && valueArray[3] !== "(")
            flag = true;
        if (flag) {
            if (valueArray.length === 3 && valueArray[2] !== "(") {
                valueArray.splice(2, 0, " (");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === 8 && valueArray[7] !== ")") {
                valueArray.splice(7, 0, ") ");
                updateView(valueArray, ngModelCtrl);
                return value;

            }
            if (valueArray.length === 13 && valueArray[12] !== " ") {
                valueArray.splice(12, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;

            }
            if (valueArray.length === 17 && valueArray[15] !== " ") {
                valueArray.splice(15, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === count + 12) {
                removeLastNumber(valueArray, ngModelCtrl);
                return value;
            }
        }
        return value;

    }
    function twoNumbersCode(value, ngModelCtrl, count) {
        var valueArray = value.split('');
        if (valueArray.length === 4 && valueArray[4] !== "(")
            flag = true;
        if (flag) {
            if (valueArray.length === 4 && valueArray[3] !== "(") {
                valueArray.splice(3, 0, " (");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === 9 && valueArray[8] !== ")") {
                valueArray.splice(8, 0, ") ");
                updateView(valueArray, ngModelCtrl);
                return value;

            }
            if (valueArray.length === 14 && valueArray[13] !== " ") {
                valueArray.splice(13, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;

            }
            if (valueArray.length === 18 && valueArray[16] !== " ") {
                valueArray.splice(16, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === count + 13) {
                removeLastNumber(valueArray, ngModelCtrl);
                return value;
            }
        }
        return value;
    }
    function threeNumbersCode(value, ngModelCtrl, count) {
        var valueArray = value.split('');
        if (valueArray.length === 5 && valueArray[5] !== "(")
            flag = true;
        if (flag) {
            if (valueArray.length === 5 && valueArray[4] !== "(") {
                valueArray.splice(4, 0, " (");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === 10 && valueArray[9] !== ")") {
                valueArray.splice(9, 0, ") ");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === 15 && valueArray[14] !== " ") {
                valueArray.splice(14, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;

            }
            if (valueArray.length === 19 && valueArray[17] !== " ") {
                valueArray.splice(17, 0, " ");
                updateView(valueArray, ngModelCtrl);
                return value;
            }
            if (valueArray.length === count + 14) {
                removeLastNumber(valueArray, ngModelCtrl);
                return value;
            }
        }
        return value;
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            defaultCountry: "@code",
            pcodeDefault: "@",
            numbersInPcode: '@',
            numbersInPhone: '@numbers'
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            element.on('keypress', function (e) {
                var char = e.char || String.fromCharCode(e.charCode);
                if (!/^[0-9]$/i.test(char)) {
                    e.preventDefault();
                    return false;
                }
            });
            element.on('blur', function () {
                if (this.value === "+" || this.value === "") {
                    this.value = "";
                }
                return this.value;
            });
            element.on('focus', function () {
                if (this.value === "") {
                    for (var i = 0; i < countries.length; i++) {
                        if (scope.defaultCountry === countries[i].code) {
                            this.value = countries[i].pcode;
                            scope.pcodeDefault = countries[i].pcode;

                        }
                    }
                }
                return this.value;
            });
            function parse(value) {
                var valueArray = value.split('');
                if (valueArray[0] !== '+') {
                    ngModelCtrl.$setViewValue('+' + value);
                    ngModelCtrl.$render();
                    flag = false;
                    return true;
                }
                if (valueArray.length === 2 || valueArray.length === 1) {
                    oneNumbersCodeFlag = false;
                    twoNumbersCodeFlag = false;
                    threeNumbersCodeFlag = false;
                }
                if (valueArray.length === 3) {
                    compareValue = valueArray[0] + valueArray[1];
                    for (var i = 0; i < countries.length; i++) {
                        if (compareValue === countries[i].pcode) {
                            oneNumbersCodeFlag = true;
                            twoNumbersCodeFlag = false;
                            threeNumbersCodeFlag = false;
                            break;
                        }

                    }
                }
                if (valueArray.length === 4) {
                    compareValue = valueArray[0] + valueArray[1] + valueArray[2];
                    for (var i = 0; i < countries.length; i++) {
                        if (compareValue === countries[i].pcode) {
                            oneNumbersCodeFlag = false;
                            twoNumbersCodeFlag = true;
                            threeNumbersCodeFlag = false;
                            break;
                        }

                    }
                }
                if (valueArray.length === 5) {
                    compareValue = valueArray[0] + valueArray[1] + valueArray[2] + valueArray[3];
                    for (var i = 0; i < countries.length; i++) {
                        if (compareValue === countries[i].pcode) {
                            oneNumbersCodeFlag = false;
                            twoNumbersCodeFlag = false;
                            threeNumbersCodeFlag = true;
                            break;
                        }

                    }
                }
                if (oneNumbersCodeFlag)
                    return oneNumbersCode(value, ngModelCtrl, scope.$eval(scope.numbersInPhone));
                if (twoNumbersCodeFlag)
                    return twoNumbersCode(value, ngModelCtrl, scope.$eval(scope.numbersInPhone));
                if (threeNumbersCodeFlag)
                    return threeNumbersCode(value, ngModelCtrl, scope.$eval(scope.numbersInPhone));


                if (valueArray.length === 6 && oneNumbersCodeFlag === false && twoNumbersCodeFlag === false && threeNumbersCodeFlag === false) {
                    removeLastNumber(valueArray, ngModelCtrl);
                    return undefined;
                }
                return value;
            }
            function format(value) {
                return value;
            }
            ngModelCtrl.$parsers.unshift(parse);
            ngModelCtrl.$formatters.unshift(format);
        }
    }
});