﻿'use strict';

angular.module("Autostation").directive("uppercaseInput", function () {
    return {
        restrict: "A",
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            function parser(value) {
                element.on('keypress', function (e) {
                    if (!/[а-я]/.test(e.key)) {
                       return false;
                    }
                });

                if (ngModel.$isEmpty(value)) {
                    return value;
                }
               var formatedValue = value.toUpperCase();
                if (ngModel.$viewValue !== formatedValue) {
                    ngModel.$setViewValue(formatedValue);
                    ngModel.$render();
                }
                return formatedValue;
            }
            function formatter(value) {
                if (ngModel.$isEmpty(value)) {
                    return value;
                }
                return value.toUpperCase();
            }
            ngModel.$formatters.unshift(formatter);
            ngModel.$parsers.unshift(parser);

        }
    }
});