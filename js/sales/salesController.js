"use strict";

angular.module("Sales", ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.bootstrap'])
  .controller("SalesController", SalesController)
  .filter('mapReturn', function() {
     var returnHash = {
        0: 'Ні',
        1: 'Так'
     };
     return function(input) {
        if ((input!==1)&&(input!==0)) {
           return '';
        } else {
           return returnHash[input];
        }
     };
  });

SalesController.$inject = ["$rootScope", "$scope", "salesService", "messageService", "stateService", "constantsService", "i18nService", "$http", "$log", "$location", "$filter", "$uibModal", "encodersService", "uiGridConstants" ];

function SalesController($rootScope, $scope, salesService, messageService, stateService, constantsService,  i18nService, $http, $log, $location, $filter, $uibModal, encodersService, uiGridConstants ) {
  var vm = this;
  i18nService.setCurrentLang('ru');
  vm.paginationOptions = {
    pageNumber: 1,
    pageSize: 15,
    sort: null
  };
  vm.filterOptions = {};

  vm.eDate= new Date();
  vm.bDate= new Date();
  vm.bDate.setDate(vm.bDate.getDate()-1);
  vm.datepickers = [];
  vm.datepickers[0]=false;
  vm.datepickers[1]=false;
  vm.ticketNum = "";
  vm.loading=false;
  vm.gridSelected=false;
  vm.entitySelected={};
  vm.styleSelected="success";
  vm.gridMessage="";

  vm.cancelInProgress=false;
  vm.cancelMessages=[];
  vm.returnInProgress=false;
  vm.returnMessages=[];
  vm.returnInfo={};

  vm.open = function(which) {
	vm.datepickers[which] = true;
  };

  vm.dateOptions = {
  	startingDay: 1
  };
 
  vm.statusStyleMain = function(grid, row, col, rowRenderIndex, colRenderIndex) {
       var status = row.entity.status;
       if ((status == "hung")||(status == "init")) {
           return 'statusInactive';
       };
       if ((status == "noticket")||(status == "canceled")||(status == "failed")||(status == "allret")) {
           return 'statusDeny';
       };
       if ((status == "success")) {
           return 'statusSuccess';
       };
       if ((status == "ticket")) {
           return 'statusConfirm';
       };
       if ((status == "hasret")) {
           return 'statusInfo';
       };
  };

  vm.gridOptions = {
    enableRowSelection: true,
    enableFullRowSelection: true,
    enableSelectAll: false,
    noUnselect: true,
    multiSelect: false,
    enableRowHeaderSelection: true,
    selectionRowHeaderWidth: 25,
    rowHeight: 25,
    paginationPageSizes: [10, 15, 20, 30, 40, 50],
    paginationPageSize: 15,
    useExternalPagination: true,
    useExternalSorting: false,
    enableSorting: false,
    enableFiltering: true,
    useExternalFiltering: true,
    expandableRowTemplate: 'views/sales/expandableRowTemplate.html',
    expandableRowHeight: 190,
    //subGridVariable will be available in subGrid scope
    expandableRowScope: {
      returnTicketClick: function(controller, rowEntity) {
                              if (rowEntity.allowOps!==1) {
                                 return;
                              };
                              $log.log(controller);
                              $log.log(rowEntity);
                              controller.openReturnDlg(rowEntity,controller);
                         },
      controller: vm
    },
    onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        vm.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          vm.paginationOptions.pageNumber = newPage;
          vm.paginationOptions.pageSize = pageSize;
          vm.getPage();
        });
        vm.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            vm.gridSelected=true;
            vm.entitySelected=row.entity;
            vm.styleSelected=vm.statusStyle(vm.entitySelected.status);
            $log.log(vm.entitySelected);
        });
        vm.gridApi.core.on.rowsRendered( $scope, function() {
            if (vm.gridMessage === "expand") {
               vm.gridMessage = "";
               vm.gridApi.expandable.expandAllRows();
            };
        });
        vm.gridApi.core.on.filterChanged( $scope, function() {
            var grid = this.grid;
            vm.filterOptions = {};
            for (var col in grid.columns) { 
               if (grid.columns[col].filters[0].$$hashKey) {
                  vm.filterOptions[grid.columns[col].name]=grid.columns[col].filters[0].term;
               };
            }
            vm.paginationOptions.pageNumber=1;
            vm.getPage();
        });
  }};
 
  vm.gridOptions.columnDefs = [
        { name: 'orderNum', displayName: 'Заказ №', width: '5%' },
        { name: 'raceNum', displayName: '№ рейсу', width: '5%' },
        { name: 'raceName', displayName: 'Найменування рейсу', width: '18%', enableFiltering: false },
        { name: 'stDepName', displayName: 'Від станції', width: '9%' },
        { name: 'stArrName', displayName: 'До станції', width: '9%' },
        { name: 'depDate', displayName: 'Відправлення', width: '8%', enableFiltering: false },
        { name: 'passName', displayName: 'Пасажир', width: '12%' },
        { name: 'statText', displayName: 'Статус', width: '10%', cellClass: vm.statusStyleMain,
          filter: {
              type: uiGridConstants.filter.SELECT,
              selectOptions: [ { value: 'hung', label: 'Завислий' }, 
                               { value: 'init', label: 'Розпочато' },
                               { value: 'ticket', label: 'Зарезервовано' },
                               { value: 'noticket', label: 'Вiдмовлено' },
                               { value: 'canceled', label: 'Скасовано' },
                               { value: 'success', label: 'Сплачено' },
                               { value: 'failed', label: 'Не сплачено' },
                               { value: 'hasret', label: 'Є повернутi' },
                               { value: 'allret', label: 'Всi повернутi' } ],
              disableCancelFilterButton: true
          }
        },
        { name: 'orderSum', displayName: 'Сума', width: '5%', enableFiltering: false },
        { name: 'agentName', displayName: 'Агент', width: '10%' },
        { name: 'oper', displayName: 'Оператор', width: '10%' },
        { name: 'Curr', displayName: 'Валюта', width: '4%', enableFiltering: false },
        { name: 'payDate', displayName: 'Час оплати', width: '8%', enableFiltering: false }
  ];

  vm.statusStyle = function (status) {
     var result="success";
     if ((status === "init")) {
       result="default";
     };
     if ((status === "noticket")||(status === "hung")) {
       result="danger";
     };
     if ((status === "canceled")||(status === "failed")||(status === "allret")||(status === "hasret")) {
       result="warning";
     };
     if ((status === "ticket")) {
       result="info";
     };
     return result;
  };

  vm.ticketNumChanged = function() {
     if ((vm.ticketNum !=="")&&(vm.ticketNum !== undefined)) {
       vm.gridOptions.enableFiltering = false;
       vm.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
       if ((vm.ticketNum.length == 10)||(vm.ticketNum.length == 13)||(vm.ticketNum.length == 16)||(vm.ticketNum.length == 17)) {
         vm.getPage();
       };
     }
     else {
       vm.gridOptions.enableFiltering = true;
       vm.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
       vm.getPage();
     }

  };

  vm.hideError = function() {
     delete $rootScope.message;
  };

  vm.searchClick = function() {
     vm.getPage();
  };

  vm.getPage = function() {
      vm.loading=true;
      var strDate1 = $filter('date')(vm.bDate, "yyyy-MM-dd");
      var strDate2 = $filter('date')(vm.eDate, "yyyy-MM-dd");
      var ticketNum = vm.ticketNum;
      var filters = encodersService.base64.encode(JSON.stringify(vm.filterOptions));
      if ((ticketNum !== "") && (ticketNum !== undefined)) {
        vm.paginationOptions.pageNumber=1;
      };
      salesService.getSales(strDate1, strDate2, ticketNum, vm.paginationOptions.pageNumber, vm.paginationOptions.pageSize, filters)
      .success(function (responce) {
        vm.loading=false;
        if (responce.error) {
           vm.gridOptions.data = [];
           if ((responce.error.code !== 'E_NODATA')||((ticketNum !== "")&&(ticketNum !== undefined))) {
             messageService.error(responce.error.name, "sales", responce.error.param);
           }
        }
        else {
           var last = responce.pop();
           vm.gridOptions.totalItems = last.orderNum;
           for(var i = 0; i < responce.length; i++){
             responce[i].subGridOptions = {
               columnDefs: [ { name: 'ticketNum', displayName: '№ квитка',
                               cellTemplate:'<a class="btn" ng-click="grid.appScope.returnTicketClick(grid.appScope.controller, row.entity)">{{row.entity.ticketNum}}</a>'
                             },
                             { field: "isReturned",
                               displayName: 'Повернений',
                               cellFilter: 'mapReturn',
                               cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                     if (grid.getCellValue(row,col) == 1) {
                                                          return 'returnedTicketColor';
                                                     }
                                          }
                             },
                             { name: 'place', displayName: 'Місце' },
                             { name: 'passName', displayName: 'Пасажир' },
                             { name: 'passPhone', displayName: 'Телефон' },
                             { name: 'passMail', displayName: 'Пошта' },
                             { name: 'price', displayName: 'Ціна' },
                             { name: 'lgota', displayName: 'Знижка' }
                           ],
               data: responce[i].orders
             }
           }
           vm.gridOptions.data = responce;
           if ((responce.length == 1) && (vm.ticketNum !== "") && (vm.ticketNum !== undefined)) {
             vm.gridMessage = "expand";
           };
           stateService.sales.save(vm);
        }
      }).error(function(responce, status) {
		vm.loading=false;
                vm.gridOptions.data = [];
                if ((status == -1)||(!responce)) {
                    messageService.error("Немає зв\'язку з сервером", "sales", "");
                } else {
                    messageService.error(responce.error.name, responce.error.type, responce.error.param);
                }
		console.log(responce);
      });
    };

    stateService.sales.restore(vm);
    vm.getPage();

};

