'use strict';
angular.module("Sales").factory("salesService", salesService);

salesService.$inject = ["$http", "constantsService"];

function salesService($http, constantsService) {

   var doRequestSales = function(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters) {
       return $http.get(constantsService.salesUrl.sales,
              { params: {
                          pageNumber: pageNumber,
                          pageSize: pageSize,
                          date1: strDate1,
                          date2: strDate2,
                          ticket: ticketNum,
                          filters: filters
                         }
              });
    }


    return {
         getSales: function(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters) {
                    return doRequestSales(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters);
         }
    };


};

