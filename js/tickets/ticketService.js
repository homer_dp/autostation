"use strict";

angular.module("Tickets")
    .factory("ticketService", ["$http", "constantsService", "$rootScope", "$filter", function ($http, constantsService, $rootScope, $filter) {
        var service = {};
        service.getCities = getCities;
        service.getRoutes = getRoutes;
        service.getStations = getStations;
        service.parseToViewRoutes = parseToViewRoutes;
        //function getCities(callback) {
        //    return $http.get(constantsService.ticketsUrl.cities).
        //        then(function successCallback(response) {
        //            callback(response);
        //        }, function errorCallback(response) {
        //            callback(response);
        //        });
        //};

        function getCities(callback) {
            return $http({
                method: "GET",
                url: constantsService.ticketsUrl.cities
            }).then(function successCallback(response) {
                callback(response);
            }, function errorCallback(response) {
                callback(response);
            });
        };


        function getRoutes(from, to, when, edges, callback) {
            return $http({
                method: "GET",
                url: constantsService.ticketsUrl.routes,
                params: { from: from, to: to, when: when, edges: edges, token: $rootScope.userGlobal.token }
            }).then(function successCallback(response) {
                callback(response);
            }, function errorCallback(response) {
                callback(response);
            });
        };

        function parseToViewRoutes(routeArray) {
            routeArray.forEach(function (entry) {
                if (entry.dtArr != "") {
                    entry.viewDtArr = parseToViewDateTime(entry.dtArr);
                }
                if (entry.dtDep != "") {
                    entry.viewDtDep = parseToViewDateTime(entry.dtDep);
                }
            });


            return routeArray;

        };

        function parseToViewDateTime(dateTime) {
            var viewDateTime = {
                viewTime: dateTime.split(' ')[1],
                viewData: dateTime.split(' ')[0],
                //viewTime: $filter('date')(new Date(dateTime.split(' ').join('T')), "HH:mm"),
                //viewData: $filter('date')(new Date(dateTime.split(' ').join('T')), "yyyy-MM-dd"),
            };
            return viewDateTime;
        };

        function getStations(routeId, callback) {
            return $http({
                method: "GET",
                url: constantsService.ticketsUrl.stations,
                params: { id: routeId }
            }).then(function successCallback(response) {
                callback(response);
            }, function errorCallback(response) {
                callback(response);
            });
        }



        return service;
    }]);
