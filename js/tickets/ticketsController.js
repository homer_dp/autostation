"use strict";
angular.module("Tickets").controller("TicketsController", TicketsController);

TicketsController.$inject = ["$scope", "ticketService", "messageService", "$filter", "$uibModal",  "$rootScope", "$location", "stateService"];

function TicketsController($scope, ticketService, messageService, $filter, $uibModal,  $rootScope, $location, stateService) {
    var vm = this;

    //Load cities with start
    (function () {
        if (stateService.trip.getTripData() !== undefined) {
            getRoutesFromOrder(stateService.trip.getTripData());
        }
        if ($rootScope.cities == undefined) {
            ticketService.getCities(function (response) {
                if (response.status === 200) {
                    vm.cities = response.data;
                    $rootScope.cities = response.data;

                } else {
                    messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
                }
            });
        } else {
            vm.cities = $rootScope.cities;
        }
       
    })();

    if ($rootScope.userGlobal.allowsale !== 1) {
      vm.allowsale = false;
    } else {
      vm.allowsale = true;
    };

    vm.sortProperty = 'id';
    vm.sortReverse = false;


    //Settings of DatePicker
    vm.popup = {
        opened: false
    };
    vm.open = function () {
        vm.popup.opened = true;

    };
    vm.dateOptions = {

        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Get routes by cities and date
    vm.getRoutes = function () {
        vm.loading = true;
        vm.dateParse = $filter('date')(vm.date, "yyyy-MM-dd");
        ticketService.getRoutes(vm.from.description.id, vm.to.description.id, vm.dateParse, vm.period, function (response) {
            if (response.status == 200) {
                if (response.data.error) {
                    vm.loading = false;
                    messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
                    vm.isTickets = false;
                } else {
                    vm.loading = false;
                    vm.routes = ticketService.parseToViewRoutes(response.data);
                    messageService.error(false, false, "");
                    vm.isTickets = true;
                }
            } else {
                vm.loading = false;
                messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
                
            }
        });
    };

    //show info for trip
    vm.showRouteOnStations = function (tripId) {
        vm.routeOnStation = true;
        ticketService.getStations(tripId, function (response) {
            if (response.status == 200) {
                var modalInstance = $uibModal.open({
                    animation: vm.animationsEnabled,
                    templateUrl: 'myModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    size: "lg",
                    resolve: {
                        stations: function () {
                            return response.data;
                        }
                    }
                });
            } else {
                messageService.error("Неможливо завантажити маршрут, спробуйте ще раз.", "NO_INFO_TRIP", "");
            }
        });
    };

    //pass tripId to orderController
    vm.passTripId = function (tripId) {
        stateService.trip.setTripId(tripId);
        if (stateService.trip.getTripData() == undefined) {
            console.log(vm.from);
            vm.ticketsOrderObject = {
                fromId: vm.from.description.id,
                fromName: vm.from.title,
                toId: vm.to.description.id,
                toName: vm.to.title,
                date: vm.dateParse,
                dateName: $filter('date')(vm.date, "dd.MM.yy"),
                period: vm.period
            }
            stateService.trip.setTripData(vm.ticketsOrderObject);
        }
        stateService.returns.set({"value":"/tickets"});
        $location.path("/order");
    };

    vm.sortBy = function(propertyName) {
        vm.sortReverse = (vm.sortProperty === propertyName) ? !vm.sortReverse : false;
        vm.sortProperty = propertyName;
    };

    function getRoutesFromOrder(data) {
        ticketService.getRoutes(data.fromId, data.toId, data.date, data.period, function (response) {
            if (response.status == 200) {
                if (response.data.error) {
                    messageService.error(response.data.error.name, "E_NODATA", response.data.error.param);
                    vm.isTickets = false;
                } else {
                    vm.routes = ticketService.parseToViewRoutes(response.data);
                    angular.element(document.getElementById("fromStation_value")).val(data.fromName);
                    angular.element(document.getElementById("toStation_value")).val(data.toName);
                    angular.element(document.getElementById("date")).val(data.dateName);
                    angular.element(document.getElementById("period")).val(data.period);
                    messageService.error(false, false, "");
                    vm.isTickets = true;
                }
            } else {
                messageService.error(response.data.error.name, "E_NODATA", response.data.error.param);
                console.log("error");
            }
        });
    };

    vm.test = function() {
        console.log("dsdsdsdsd");
    };

};
