﻿'use strict';

angular.module("Reserve").controller("ReserveController", ReserveController);
ReserveController.$inject = ["$scope", "stateService", "$location"];

function ReserveController($scope, stateService, $location) {
    var vm = this;
    vm.returnCopyStyle={};

    (function () {
        if (stateService.reserve.getReserve() !== undefined) {
            vm.reserve = stateService.reserve.getReserve();
            vm.buyId = stateService.reserve.getBuyId();
        }
        if (vm.reserve.isCopy==1) {
          vm.returnCopyStyle={'background-size':'contain,contain', 'background-image':'url(pictures/copy.png)'};
        }
        else {
          vm.returnCopyStyle={};
        }
        //console.log(vm.reserve);
     })();
   
    vm.print = function() {
        window.print();
    }

    vm.backFromTicket = function() {
        var locationStr = stateService.returns.get();
        if (locationStr.value) {
          $location.path(locationStr.value); 
        }
        else {
          $location.path("/tickets");
        }
        stateService.trip.removeTripInfo();
    }




};