﻿'use strict';
angular.module("Order").factory("orderService", orderService);

orderService.$inject = ["$http", "constantsService", "encodersService", "$rootScope", "$filter"];
function orderService($http, constantsService, encodersService, $rootScope, $filter) {
    var service = {};
    service.getTrip = getTrip;
    service.parseToViewTrip = parseToViewTrip;
    service.buyInit = buyInit;
    service.buy = buy;


    function getTrip(tripId, callback) {
        return $http({
            method: "GET",
            url: constantsService.orderUrl.trip,
            params: { "id": tripId }
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(response) {
            callback(response);
        });
    };

    function buyInit(callback) {
        return $http({
            method: "GET",
            url: constantsService.orderUrl.buyinint
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(response) {
            callback(response);
        });
    };

    function buy(tripId, data, buyId, reserve, callback) {
        return $http({
            method: "GET",
            url: constantsService.orderUrl.buy,
            params:{"id": tripId, "places": encodersService.base64.encode(JSON.stringify(data)), "buyid": buyId, "reserve": reserve }
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(response) {
            callback(response);
        });
    };




    function parseToViewTrip(routeArray) {

        if (routeArray.dtArr != "") {
            routeArray.viewDtArr = parseToViewDateTime(routeArray.dtArr);
        }
        if (routeArray.dtDep != "") {
            routeArray.viewDtDep = parseToViewDateTime(routeArray.dtDep);
        }
        return routeArray;

    };

    function parseToViewDateTime(dateTime) {
        var viewDateTime = {
            viewTime: dateTime.split(' ')[1],
            viewData: dateTime.split(' ')[0],
            //viewTime: $filter('date')(new Date(dateTime.split(' ').join('T')), "HH:mm"),
            //viewData: $filter('date')(new Date(dateTime.split(' ').join('T')), "yyyy-MM-dd"),
        };
        return viewDateTime;
    };

    return service;
};




