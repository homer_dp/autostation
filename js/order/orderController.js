﻿"use strict";
angular.module("Order").controller("OrderController", OrderController);

OrderController.$inject = ["$scope", "orderService", "messageService", "stateService", "$location",  "$rootScope"];
function OrderController($scope, orderService, messageService, stateService, $location, $rootScope) {
    var vm = this;
    (function () {

        orderService.getTrip(stateService.trip.getTripId(), function (response) {
            if (response.status == 200) {
                vm.trip = orderService.parseToViewTrip(response.data);
                createMatrix(vm.trip.busCapacity);
                if (vm.fullPrice === undefined)
                    vm.fullPrice = vm.trip.pricePass;

            } else {
                messageService.error(response.data.error.name, response.data.error.type, response.data.error.param);
            }
            vm.firstShowFullPrice = false;
            vm.fullPriceArray = [];
        });
    })();

    //settings for ui.mask
    vm.phonenumber = "";
    vm.passport = "";
    vm.optionsPassport = {
        maskDefinitions: { '9': /\d/, 'A': /[А-Я]/, '*': /[a-zA-Z0-9]/ },
        clearOnBlur: true,
        clearOnBlurPlaceholder: true,
        eventsToHandle: ['input', 'keyup', 'click', 'focus'],
        addDefaultPlaceholder: true,
        escChar: '\\',
        allowInvalidValue: false

    };

    //making order
    vm.packageArray = [1, 2, 3, 4];
    vm.ordersArray = [];
    vm.backupArray = [];

    vm.selectPlace = function (placeNumber) {
        var count = 0;
        var arrayIndex = -1, fullPriceIndex = -1;
        var delElem = [];
        var i;
        arrayIndex = getIndex(vm.ordersArray, placeNumber);

        var elem = angular.element(document.getElementById(placeNumber));
        if (elem.hasClass("busselected")) {
            elem.removeClass("busselected");
            if (arrayIndex >= 0) {
                delElem = vm.ordersArray.splice(arrayIndex, 1);
                fullPriceIndex = getIndex(vm.fullPriceArray, placeNumber);
                vm.fullPriceArray.splice(fullPriceIndex, 1);
                checkFullPriceAfterChangePlace(vm.fullPriceArray, count);
                if (delElem[0].place >= 0) {
                    vm.backupArray[delElem[0].place] = delElem[0];
                }
            }
        }
        else {
            elem.addClass("busselected");
            if (arrayIndex < 0) {
                if ((placeNumber >= 0) && (vm.backupArray[placeNumber])) {
                    vm.ordersArray.push(vm.backupArray[placeNumber]);
                }
                else {
                    vm.ordersArray.push({ place: placeNumber, name: "", lastName: "", phone: "", email: "", passport: "", lgot: "", lgPrice: "", baggage: "", price: vm.trip.pricePass });
                    vm.fullPriceArray.push({ place: placeNumber, price: vm.trip.pricePass });
                    checkFullPriceAfterChangePlace(vm.fullPriceArray, count);
                }
            }
        }
        vm.isOrder = (vm.ordersArray.length > 0);

    }


    vm.changePrice = function (place, lgotPrice, packagesCount, index) {
        var count = 0;
        if (!vm.firstShowFullPrice) {
            vm.fullPrice = 0;
            vm.firstShowFullPrice = true;
        }
        console.log(place, lgotPrice, packagesCount);
        var singlePriceIndex = -1, fullPriceIndex = -1;
        singlePriceIndex = getIndex(vm.ordersArray, place);
        fullPriceIndex = getIndex(vm.fullPriceArray, place);
        console.log(lgotPrice);
        if (lgotPrice !== null) {
            if (lgotPrice.price !== "") {
                vm.ordersArray[singlePriceIndex].price = (lgotPrice.price + (vm.trip.priceBagg * packagesCount)).toFixed(2);
                vm.ordersArray[singlePriceIndex].lgot = lgotPrice.code;
                vm.fullPriceArray[fullPriceIndex].price = (parseFloat(vm.ordersArray[singlePriceIndex].price)).toFixed(2);
            } else {
                vm.ordersArray[singlePriceIndex].price = (parseFloat(vm.trip.pricePass) + parseFloat((vm.trip.priceBagg * packagesCount))).toFixed(2);
                vm.fullPriceArray[fullPriceIndex].price = (parseFloat(vm.ordersArray[singlePriceIndex].price)).toFixed(2);
                vm.ordersArray[singlePriceIndex].lgot = "";
            }
        } else {
            vm.ordersArray[singlePriceIndex].price = (parseFloat(vm.trip.pricePass) + parseFloat((vm.trip.priceBagg * packagesCount))).toFixed(2);
            vm.fullPriceArray[fullPriceIndex].price = (parseFloat(vm.ordersArray[singlePriceIndex].price)).toFixed(2);
            vm.ordersArray[singlePriceIndex].lgot = "";
        }

        updateFullPrice(vm.fullPriceArray);
        //placeholder for selects
        var selectPackage = angular.element(document.getElementById("package" + index));
        var selectLgot = angular.element(document.getElementById("lgots" + index));
        if (selectPackage.val() === "")
            selectPackage.addClass("notChosen");

        else
            selectPackage.removeClass("notChosen");
        if (selectLgot.val() === "")
            selectLgot.addClass("notChosen");

        else
            selectLgot.removeClass("notChosen");

    }


    ///////////////////


    function checkFullPriceAfterChangePlace(pricesArray, temp) {
        for (var i = 0; i < pricesArray.length; i++) {
            temp += parseFloat(pricesArray[i].price);
            if (isNaN(temp) === true)
                vm.fullPrice = vm.trip.pricePass;
            else
                vm.fullPrice = temp;
        }
    }

    function updateFullPrice(pricesArray) {
        var count = 0;
        for (var i = 0; i < pricesArray.length; i++) {
            count += parseFloat(pricesArray[i].price);
            vm.fullPrice = count;
        }
    }

    function getIndex(array, item) {
        var index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].place === item) {
                index = i;
                break;
            }
        }
        return index;
    }


    function createMatrix(count) {
        vm.count = count;
        vm.matrix = new Array();
        var i, j = 0;
        for (i = 0; i <= 3; i++) {
            vm.matrix[i] = new Array();
            for (j = 0; j < (vm.count / 4) ; j++) {
                if ((i + (j * 4) + 1) <= vm.count) vm.matrix[i][j] = i + (j * 4) + 1;
            }
        }
    }



    vm.freePlaces = function (item) {
        var vm = this;
        var found = false;
        for (var i = 0; i < vm.trip.places.length; i++) {
            if (vm.trip.places[i].num === item) {
                found = true;
                break;
            }
        }
        return found;
    };



    //Make order
    vm.makeOrder = function (reserve) {
        console.log(vm.ordersArray);
        vm.loading = true;
        if (reserve != 1) reserve=0;
        orderService.buyInit(function (responseInit) {
            if (responseInit.status === 200) {
                orderService.buy(vm.trip.tripId, vm.ordersArray, responseInit.data.buyid, reserve, function (responseBuy) {
                    console.log(vm.ordersArray);
                    if (responseBuy.status === 200) {
                        vm.loading = false;
                        stateService.returns.set({"value":"/tickets"});
                        if (reserve == 1) {
                           stateService.reserve.setReserve(responseBuy.data);
                           stateService.reserve.setBuyId(responseInit.data.buyid);
                           $location.path("/reserve");
                        }
                        else {
                           stateService.ticket.setTicket(responseBuy.data);
                           stateService.ticket.setBuyId(responseInit.data.buyid);
                           $location.path("/ticket");
                        }
                    } else {
                        vm.loading = false;
                        messageService.error(responseBuy.data.error.name, responseBuy.data.error.type, responseBuy.data.error.param);
                    }
                });
            } else {
                vm.loading = false;
                if (responseInit.status === -1) {
                    messageService.error("Немає зв\'язку з сервером", "NO_CONNECTION", "");
                } else {
                    messageService.error(responseInit.data.error.name, responseInit.data.error.type, responseInit.data.error.param);
                }
            }

        });
    };


    //////////////


    vm.goToTrips = function () {
        var locationStr = stateService.returns.get();
        if (locationStr.value) {
          $location.path(locationStr.value); 
        }
        else {
          $location.path("/tickets");
        }
    }

    vm.countryCodes = [
        { name: "Albania", code: "AL", pcode: "+355" },
        { name: "Armenia", code: "AM", pcode: "++374" },
        { name: "Austria", code: "AT", pcode: "+43" },
        { name: "Azerbaijan", code: "AZ", pcode: "+994" },
        { name: "Belarus", code: "BY", pcode: "+375" },
        { name: "Belgium", code: "BE", pcode: "+32" },
        { name: "Bosnia and Herzegovina", code: "BA", pcode: "+387" },
        { name: "Bulgaria", code: "BG", pcode: "+359" },
        { name: "Croatia", code: "HR", pcode: "+385" },
        { name: "Cyprus", code: "CY", pcode: "+357" },
        { name: "Czech Republic", code: "CZ", pcode: "+420" },
        { name: "Denmark", code: "DK", pcode: "+45" },
        { name: "Egypt", code: "EG", pcode: "+20" },
        { name: "Estonia", code: "EE", pcode: "+372" },
        { name: "Finland", code: "FI", pcode: "+358" },
        { name: "France", code: "FR", pcode: "+33" },
        { name: "Georgia", code: "GE", pcode: "+995" },
        { name: "Germany", code: "DE", pcode: "+49" },
        { name: "Greece", code: "GR", pcode: "+30" },
        { name: "Hungary", code: "HU", pcode: "+36" },
        { name: "Ireland", code: "IE", pcode: "+353" },
        { name: "Israel", code: "IL", pcode: "+972" },
        { name: "Italy", code: "IT", pcode: "+39" },
        { name: "Jordan", code: "JO", pcode: "+962" },
        { name: "Kazakhstan", code: "KZ", pcode: "+7" },
        { name: "Kosovo", code: "XK", pcode: "+383" },
        { name: "Kyrgyzstan", code: "KG", pcode: "+996" },
        { name: "Latvia", code: "LV", pcode: "+371" },
        { name: "Liechtenstein", code: "LI", pcode: "+423" },
        { name: "Lithuania", code: "LT", pcode: "+370" },
        { name: "Luxembourg", code: "LU", pcode: "+352" },
        { name: "Macedonia", code: "MK", pcode: "+389" },
        { name: "Moldova", code: "MD", pcode: "+373" },
        { name: "Monaco", code: "MC", pcode: "+377" },
        { name: "Netherlands", code: "NL", pcode: "+31" },
        { name: "Norway", code: "NO", pcode: "+47" },
        { name: "Palestine", code: "PS", pcode: "+970" },
        { name: "Poland", code: "PL", pcode: "+48" },
        { name: "Portugal", code: "PT", pcode: "+351" },
        { name: "Romania", code: "RO", pcode: "+40" },
        { name: "Russia", code: "RU", pcode: "+7" },
        { name: "San Marino", code: "SM", pcode: "+378" },
        { name: "Serbia", code: "RS", pcode: "+381" },
        { name: "Sierra Leone", code: "SL", pcode: "+232" },
        { name: "Slovakia", code: "SK", pcode: "+421" },
        { name: "Slovenia", code: "SI", pcode: "+386" },
        { name: "Somalia", code: "SO", pcode: "+252" },
        { name: "Spain", code: "ES", pcode: "+34" },
        { name: "Swaziland", code: "SZ", pcode: "+268" },
        { name: "Sweden", code: "SE", pcode: "+46" },
        { name: "Switzerland", code: "CH", pcode: "+41" },
        { name: "Syria", code: "SY", pcode: "+963" },
        { name: "Tajikistan", code: "TJ", pcode: "+992" },
        { name: "Tonga", code: "TO", pcode: "+676" },
        { name: "Turkey", code: "TR", pcode: "+90" },
        { name: "Turkmenistan", code: "TM", pcode: "+993" },
        { name: "Uganda", code: "UG", pcode: "+256" },
        { name: "Ukraine", code: "UA", pcode: "+38" },
        { name: "United Arab Emirates", pcode: "+AE", code: "971" },
        { name: "United Kingdom", code: "GB", pcode: "+44" },
        { name: "Uzbekistan", code: "UZ", pcode: "+998" },
        { name: "Vatican", code: "VA", pcode: "+379" }
    ];


}


