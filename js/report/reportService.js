'use strict';
angular.module("Report").factory("reportService", reportService);

reportService.$inject = ["$http", "constantsService"];

function reportService($http, constantsService) {

   var doRequestReportXls = function(strDate1, strDate2, reportId) {
       return $http.get(constantsService.reportUrl.reportXls,
              { params: {
                          date1: strDate1,
                          date2: strDate2,
                          report: reportId
                         },
                responseType: 'arraybuffer'
              });
    }


    return {
         getReportXls: function(strDate1, strDate2, reportId) {
                    return doRequestReportXls(strDate1, strDate2, reportId);
         }
    };


};

