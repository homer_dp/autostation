"use strict";

angular.module("Report", ['ngTouch', 'ui.bootstrap'])
  .controller("ReportController", ReportController)

ReportController.$inject = ["$rootScope", "$scope", "reportService", "messageService", "stateService", "constantsService", "$http", "$log", "$location", "$filter" ];

function ReportController($rootScope, $scope, reportService, messageService, stateService, constantsService, $http, $log, $location, $filter ) {
  var vm = this;

  vm.eDate= new Date();
  vm.bDate= new Date();
  vm.bDate.setDate(vm.bDate.getDate()-1);
  vm.datepickers = [];
  vm.datepickers[0]=false;
  vm.datepickers[1]=false;
  vm.loading=false;
  vm.reportId="";
  vm.usermode=$rootScope.userGlobal.mode;

  vm.open = function(which) {
	vm.datepickers[which] = true;
  };

  vm.dateOptions = {
  	startingDay: 1
  };

  vm.hideError = function() {
     delete $rootScope.message;
  };

  vm.reportClick = function() {
     stateService.report.save(vm);
     vm.getReport();
  };

  vm.getReport = function() {
         vm.loading=true;
         var strDate1 = $filter('date')(vm.bDate, "yyyy-MM-dd");
         var strDate2 = $filter('date')(vm.eDate, "yyyy-MM-dd");
         reportService.getReportXls(strDate1, strDate2, vm.reportId)
         .success(function (response, status, header, config) {
           vm.loading=false;
           var fileName = "report_"+vm.reportId+"_"+$filter('date')(vm.bDate, "dd.MM.yy")+"_"+$filter('date')(vm.eDate, "dd.MM.yy")+".xlsx";
           var saveData = (function () {
               var a = document.createElement("a");
               document.body.appendChild(a);
               a.style = "display: none";
               return function (data, fileName) {
                   var blob = new Blob([data], {type: "application/octet-stream"});
                   var url_ = window.webkitURL || window.URL;
                   var url = url_.createObjectURL(blob);
                   a.href = url;
                   a.download = fileName;
                   a.click();
                   url_.revokeObjectURL(url);
               };
           }());
           saveData(response, fileName);
         }).error(function(response, status) {
	   vm.loading=false;
              if ((status == -1)||(!response)) {
                  messageService.error("Немає зв\'язку з сервером", "race", "");
              } else {
                  var dataView = new DataView(response);
                  var decoder = new TextDecoder("utf-8");
                  var decodedString = decoder.decode(dataView);
                  response = JSON.parse(decodedString);
                  messageService.error(response.error.name, response.error.type, response.error.param);
              }
         });
  };

  stateService.report.restore(vm);

};

