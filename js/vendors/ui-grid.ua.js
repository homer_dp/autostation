/**
 * Created by Vitaly Kvirikadze on 2016-06-09.
 */
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('ua', {
        headerCell: {
          aria: {
            defaultFilterLabel: 'Фiльтр стовпчика',
            removeFilter: 'Видалити фiльтр',
            columnMenuButtonLabel: 'Меню ствпчика'
          },
          priority: 'Прiоритет:',
          filterLabel: "Фiльтр стовпчика: "
        },
        aggregate: {
          label: 'елементи'
        },
        groupPanel: {
          description: 'Для групування за стовпчиком перетягнiть сюди його назву.'
        },
        search: {
          placeholder: 'Пошук...',
          showingItems: 'Показати елементи:',
          selectedItems: 'Обранi елементи:',
          totalItems: 'Усього елементiв:',
          size: 'Розмiр сторiнки:',
          first: 'Перша сторiнка',
          next: 'Наступна сторiнка',
          previous: 'Попередня сторiнка',
          last: 'Остання сторiнка'
        },
        menu: {
          text: 'Обрати ствпчики:'
        },
        sort: {
          ascending: 'За зростанням',
          descending: 'За спаданням',
          none: 'Без сортування',
          remove: 'Прибрати сортування'
        },
        column: {
          hide: 'Приховати стовпчик'
        },
        aggregation: {
          count: 'усього рядкiв: ',
          sum: 'iтого: ',
          avg: 'середнє: ',
          min: 'мiн: ',
          max: 'макс: '
        },
				pinning: {
					pinLeft: 'Закрiпити лiворуч',
					pinRight: 'Закрiпити праворуч',
					unpin: 'Вiдкрiпити'
				},
        columnMenu: {
          close: 'Закрити'
        },
        gridMenu: {
          aria: {
            buttonLabel: 'Меню'
          },
          columns: 'Стовпчики:',
          importerTitle: 'iмпортувати файл',
          exporterAllAsCsv: 'Експортувати все в CSV',
          exporterVisibleAsCsv: 'Експортувати видимi данi в CSV',
          exporterSelectedAsCsv: 'Експортувати обранi данi в CSV',
          exporterAllAsPdf: 'Експортувати все в PDF',
          exporterVisibleAsPdf: 'Експортувати видимi данi в PDF',
          exporterSelectedAsPdf: 'Експортувати обранi данi в PDF',
          clearAllFilters: 'Очистити всi фiльтри'
        },
        importer: {
          noHeaders: 'Не вдалося отримати назви стовпчикiв, чи є в файлi заголовок?',
          noObjects: 'Не вдалося отримати данi, чи є в файлi рядки окрiм заголовка?',
          invalidCsv: 'Не вдалося обробити файл, чи це коректний CSV-файл?',
          invalidJson: 'Не вдалося обробити файл, чи це коректний JSON?',
          jsonNotArray: 'JSON-файл що iмпортується повинен мiстити масив, операцiю скасовано.'
        },
        pagination: {
          aria: {
            pageToFirst: 'Перша сторiнка',
            pageBack: 'Попередня сторiнка',
            pageSelected: 'Обрана сторiнка',
            pageForward: 'Наступна сторiнка',
            pageToLast: 'Остання сторiнка'
          },
          sizes: 'рядкiв на сторiнку',
          totalItems: 'рядкiв',
          through: 'по',
          of: 'з'
        },
        grouping: {
          group: 'Групувати',
          ungroup: 'Розгрупувати',
          aggregate_count: 'Групувати: Кiлькiсть',
          aggregate_sum: 'Для групи: Сума',
          aggregate_max: 'Для групи: Максимум',
          aggregate_min: 'Для групи: Мiнiмум',
          aggregate_avg: 'Для групи: Серднє',
          aggregate_remove: 'Для групи: Пусто'
        }
      });
      return $delegate;
    }]);
  }]);
})();
