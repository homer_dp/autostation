'use strict';
angular.module("Orders").factory("ordersService", ordersService);

ordersService.$inject = ["$http", "constantsService"];

function ordersService($http, constantsService) {

   var doRequestOrders = function(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters) {
       return $http.get(constantsService.ordersUrl.orders,
              { params: {
                          pageNumber: pageNumber,
                          pageSize: pageSize,
                          date1: strDate1,
                          date2: strDate2,
                          ticket: ticketNum,
                          filters: filters
                         }
              });
    }

   var doRequestRebuy = function(buyid) {
       return $http.get(constantsService.orderUrl.buy,
              { params: {
                          buyid: buyid
                         }
              });
    }

   var doRequestCancel = function(buyid) {
       return $http.get(constantsService.ordersUrl.cancel,
              { params: {
                          buyid: buyid
                         }
              });
    }

   var doRequestRetinfo = function(buyid, ticket) {
       return $http.get(constantsService.ordersUrl.retinfo,
              { params: {
                          buyid: buyid,
                          ticket: ticket
                         }
              });
    }

   var doRequestReturn = function(buyid, ticket, percent, copy) {
       return $http.get(constantsService.ordersUrl.return_,
              { params: {
                          buyid: buyid,
                          ticket: ticket,
                          percent: percent,
                          copy: copy
                         }
              });
    }

   var doRequestExportPdf = function(buyid) {
       return $http.get(constantsService.orderUrl.exportPdf,
              { params: {
                          buyid: buyid
                         }
              });
    }


    return {
         getOrders: function(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters) {
                    return doRequestOrders(strDate1, strDate2, ticketNum, pageNumber, pageSize, filters);
         },
         getRebuy: function(buyid) {
                    return doRequestRebuy(buyid);
         },
         getCancel: function(buyid) {
                    return doRequestCancel(buyid);
         },
         getRetinfo: function(buyid, ticket) {
                    return doRequestRetinfo(buyid, ticket);
         },
         getReturn: function(buyid, ticket, percent, copy) {
                    return doRequestReturn(buyid, ticket, percent, copy);
         },
         getExportPdf: function(buyid) {
                    return doRequestExportPdf(buyid);
         }
    };


};

