"use strict";

angular.module("Orders", ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.bootstrap'])
  .controller("ModalController", ModalController)
  .controller("ModalControllerCancel", ModalControllerCancel)
  .controller("ModalControllerPay", ModalControllerPay)
  .controller("OrdersController", OrdersController)
  .filter('mapReturn', function() {
     var returnHash = {
        0: 'Ні',
        1: 'Так'
     };
     return function(input) {
        if ((input!==1)&&(input!==0)) {
           return '';
        } else {
           return returnHash[input];
        }
     };
  });

OrdersController.$inject = ["$rootScope", "$scope", "ordersService", "messageService", "stateService", "constantsService", "i18nService", "$http", "$log", "$location", "$filter", "$uibModal", "encodersService", "uiGridConstants" ];
ModalController.$inject = ["$scope", "$uibModal", "$uibModalInstance", "$log", "param"];
ModalControllerCancel.$inject = ["$scope", "$uibModal", "$uibModalInstance", "$log", "ordersService", "param"];
ModalControllerPay.$inject = ["$scope", "$uibModal", "$uibModalInstance", "$log", "ordersService", "orderService", "stateService", "$location", "param"];

function OrdersController($rootScope, $scope, ordersService, messageService, stateService, constantsService,  i18nService, $http, $log, $location, $filter, $uibModal, encodersService, uiGridConstants ) {
  var vm = this;
  i18nService.setCurrentLang('ru');
  vm.paginationOptions = {
    pageNumber: 1,
    pageSize: 15,
    sort: null
  };
  vm.filterOptions = {};

  vm.eDate= new Date();
  vm.bDate= new Date();
  vm.bDate.setDate(vm.bDate.getDate()-1);
  vm.datepickers = [];
  vm.datepickers[0]=false;
  vm.datepickers[1]=false;
  vm.ticketNum = "";
  vm.loading=false;
  vm.gridSelected=false;
  vm.entitySelected={};
  vm.styleSelected="success";
  vm.gridMessage="";

  vm.cancelInProgress=false;
  vm.cancelMessages=[];
  vm.returnInProgress=false;
  vm.returnMessages=[];
  vm.returnInfo={};

  vm.open = function(which) {
	vm.datepickers[which] = true;
  };

  vm.dateOptions = {
  	startingDay: 1
  };
 
  vm.statusStyleMain = function(grid, row, col, rowRenderIndex, colRenderIndex) {
       var status = row.entity.status;
       if ((status == "hung")||(status == "init")) {
           return 'statusInactive';
       };
       if ((status == "noticket")||(status == "canceled")||(status == "failed")||(status == "allret")) {
           return 'statusDeny';
       };
       if ((status == "success")) {
           return 'statusSuccess';
       };
       if ((status == "ticket")) {
           return 'statusConfirm';
       };
       if ((status == "hasret")) {
           return 'statusInfo';
       };
  };

  vm.gridOptions = {
    enableRowSelection: true,
    enableFullRowSelection: true,
    enableSelectAll: false,
    noUnselect: true,
    multiSelect: false,
    enableRowHeaderSelection: true,
    selectionRowHeaderWidth: 25,
    rowHeight: 25,
    paginationPageSizes: [10, 15, 20, 30, 40, 50],
    paginationPageSize: 15,
    useExternalPagination: true,
    useExternalSorting: false,
    enableSorting: false,
    enableFiltering: true,
    useExternalFiltering: true,
    expandableRowTemplate: 'views/orders/expandableRowTemplate.html',
    expandableRowHeight: 190,
    //subGridVariable will be available in subGrid scope
    expandableRowScope: {
      returnTicketClick: function(controller, rowEntity) {
                              if (rowEntity.allowOps!==1) {
                                 return;
                              };
                              $log.log(controller);
                              $log.log(rowEntity);
                              controller.openReturnDlg(rowEntity,controller);
                         },
      controller: vm
    },
    onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        vm.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          vm.paginationOptions.pageNumber = newPage;
          vm.paginationOptions.pageSize = pageSize;
          vm.getPage();
        });
        vm.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            vm.gridSelected=true;
            vm.entitySelected=row.entity;
            vm.styleSelected=vm.statusStyle(vm.entitySelected.status);
            $log.log(vm.entitySelected);
        });
        vm.gridApi.core.on.rowsRendered( $scope, function() {
            if (vm.gridMessage === "expand") {
               vm.gridMessage = "";
               vm.gridApi.expandable.expandAllRows();
            };
        });
        vm.gridApi.core.on.filterChanged( $scope, function() {
            var grid = this.grid;
            vm.filterOptions = {};
            for (var col in grid.columns) { 
               if (grid.columns[col].filters[0].$$hashKey) {
                  vm.filterOptions[grid.columns[col].name]=grid.columns[col].filters[0].term;
               };
            }
            vm.paginationOptions.pageNumber=1;
            vm.getPage();
        });
  }};
 
  vm.gridOptions.columnDefs = [
        { name: 'orderNum', displayName: 'Заказ №', width: '6%' },
        { name: 'raceNum', displayName: '№ рейсу', width: '7%' },
        { name: 'raceName', displayName: 'Найменування рейсу', width: '18%', enableFiltering: false },
        { name: 'stDepName', displayName: 'Від станції', width: '9%' },
        { name: 'stArrName', displayName: 'До станції', width: '9%' },
        { name: 'payDate', displayName: 'Час оплати', width: '8%', enableFiltering: false },
        { name: 'depDate', displayName: 'Відправлення', width: '8%', enableFiltering: false },
        { name: 'passName', displayName: 'Пасажир', width: '12%' },
        //{ name: 'passPhone', displayName: 'Телефон', width: '9%' },
        { name: 'oper', displayName: 'Оператор', width: '10%' },
        { name: 'statText', displayName: 'Статус', width: '10%', cellClass: vm.statusStyleMain,
          filter: {
              type: uiGridConstants.filter.SELECT,
              selectOptions: [ { value: 'hung', label: 'Завислий' }, 
                               { value: 'init', label: 'Розпочато' },
                               { value: 'ticket', label: 'Зарезервовано' },
                               { value: 'noticket', label: 'Вiдмовлено' },
                               { value: 'canceled', label: 'Скасовано' },
                               { value: 'success', label: 'Сплачено' },
                               { value: 'failed', label: 'Не сплачено' },
                               { value: 'hasret', label: 'Є повернутi' },
                               { value: 'allret', label: 'Всi повернутi' } ],
              disableCancelFilterButton: true
          }
        },
        { name: 'orderSum', displayName: 'Сума', width: '5%', enableFiltering: false },
        { name: 'Curr', displayName: 'Валюта', width: '4%', enableFiltering: false }
  ];

  vm.statusStyle = function (status) {
     var result="success";
     if ((status === "init")) {
       result="default";
     };
     if ((status === "noticket")||(status === "hung")) {
       result="danger";
     };
     if ((status === "canceled")||(status === "failed")||(status === "allret")||(status === "hasret")) {
       result="warning";
     };
     if ((status === "ticket")) {
       result="info";
     };
     return result;
  };

  vm.ticketNumChanged = function() {
     if ((vm.ticketNum !=="")&&(vm.ticketNum !== undefined)) {
       vm.gridOptions.enableFiltering = false;
       vm.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
       if ((vm.ticketNum.length == 10)||(vm.ticketNum.length == 13)||(vm.ticketNum.length == 16)||(vm.ticketNum.length == 17)) {
         vm.getPage();
       };
     }
     else {
       vm.gridOptions.enableFiltering = true;
       vm.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
       vm.getPage();
     }

  };

  vm.hideError = function() {
     delete $rootScope.message;
  };

  vm.searchClick = function() {
     vm.getPage();
  };

  vm.loadPdf = function() {
     return constantsService.orderUrl.exportPdf+'?token='+$rootScope.userGlobal.token+'&buyid='+vm.entitySelected.buyId;
  }

  vm.getPage = function() {
      vm.loading=true;
      var strDate1 = $filter('date')(vm.bDate, "yyyy-MM-dd");
      var strDate2 = $filter('date')(vm.eDate, "yyyy-MM-dd");
      var ticketNum = vm.ticketNum;
      var filters = encodersService.base64.encode(JSON.stringify(vm.filterOptions));
      if ((ticketNum !== "") && (ticketNum !== undefined)) {
        vm.paginationOptions.pageNumber=1;
      };
      ordersService.getOrders(strDate1, strDate2, ticketNum, vm.paginationOptions.pageNumber, vm.paginationOptions.pageSize, filters)
      .success(function (responce) {
        vm.loading=false;
        if (responce.error) {
           vm.gridOptions.data = [];
           if ((responce.error.code !== 'E_NODATA')||((ticketNum !== "")&&(ticketNum !== undefined))) {
             messageService.error(responce.error.name, "orders", responce.error.param);
           }
        }
        else {
           var last = responce.pop();
           vm.gridOptions.totalItems = last.orderNum;
           for(var i = 0; i < responce.length; i++){
             responce[i].subGridOptions = {
               columnDefs: [ { name: 'ticketNum', displayName: '№ квитка',
                               cellTemplate:'<a class="btn" ng-click="grid.appScope.returnTicketClick(grid.appScope.controller, row.entity)">{{row.entity.ticketNum}}</a>'
                             },
                             { field: "isReturned",
                               displayName: 'Повернений',
                               cellFilter: 'mapReturn',
                               cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                     if (grid.getCellValue(row,col) == 1) {
                                                          return 'returnedTicketColor';
                                                     }
                                          }
                             },
                             { name: 'place', displayName: 'Місце' },
                             { name: 'passName', displayName: 'Пасажир' },
                             { name: 'passPhone', displayName: 'Телефон' },
                             { name: 'passMail', displayName: 'Пошта' },
                             { name: 'price', displayName: 'Ціна' },
                             { name: 'lgota', displayName: 'Знижка' }
                           ],
               data: responce[i].orders
             }
           }
           vm.gridOptions.data = responce;
           if ((responce.length == 1) && (vm.ticketNum !== "") && (vm.ticketNum !== undefined)) {
             vm.gridMessage = "expand";
           };
           stateService.orders.save(vm);
        }
      }).error(function(responce, status) {
		vm.loading=false;
                vm.gridOptions.data = [];
                if ((status == -1)||(!responce)) {
                    messageService.error("Немає зв\'язку з сервером", "orders", "");
                } else {
                    messageService.error(responce.error.name, responce.error.type, responce.error.param);
                }
		console.log(responce);
      });
    };

  vm.openPayDlg = function () {
    var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        templateUrl: 'views/orders/payDialogTemplate.html',
        controller: 'ModalControllerPay',
        controllerAs: 'payCtrl',
        resolve: {
           param: function () {
               return {
                 'entitySelected':  vm.entitySelected
               };
           }
         }
      });
      modalInstance.result.then(function () {
      }, function () {
        vm.getPage();
      });
  };


  vm.openCancelDlg = function () {
    var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        templateUrl: 'views/orders/cancelDialogTemplate.html',
        controller: 'ModalControllerCancel',
        controllerAs: 'cancelCtrl',
        resolve: {
           param: function () {
               return {
                 'entitySelected':     vm.entitySelected
               };
           }
         }
      });
      modalInstance.result.then(function () {
      }, function () {
        vm.getPage();
      });
  };

  vm.openReturnDlg = function (rowEntity,controller) {
      var vm = controller;
      var buyid = vm.entitySelected.buyId;
      var ticket = rowEntity.ticketNum;
      var isCopy = rowEntity.isReturned;
      vm.returnInfo={};
      if (!isCopy) {
          ordersService.getRetinfo(buyid,ticket)
          .success(function (response) {
            vm.loading=false;
            if (response.error) {
               vm.loading=false;
               messageService.error(response.error.name, response.error.type, response.error.param);
            }
            else {
               vm.loading=false;
               vm.returnInfo=response;
               vm.returnSelected=null;

               var modalInstance = $uibModal.open({
                   animation: false,
                   size: 'lg',
                   templateUrl: 'views/orders/returnDialogTemplate.html',
                   controller: 'ModalController',
                   controllerAs: 'returnCtrl',
                   resolve: {
                      param: function () {
                          return {
                            'returnInfo':     vm.returnInfo
                            //'entireScope':    vm
                          };
                      }
                    }
                 });
                 modalInstance.result.then(function (selectedItem) {
                   if (selectedItem) {
                     vm.doReturn(selectedItem, buyid, ticket, 0); 
                   }
                 }, function () {
                   $log.info('Modal dismissed at: ' + new Date());
                 });
           }

        
          }).error(function(response, status) {
		vm.loading=false;
                messageService.error(response.error.name, response.error.type, response.error.param);
		console.log(response);
          });

      }
      else {
         vm.doReturn({"percId":""}, buyid, ticket, 1); 
      }
  };

  vm.doReturn = function (selectedItem, buyid, ticket, isCopy) {
      vm.loading=true;
      if (!selectedItem) {
         return;
      }
      var percent = selectedItem.percId;
      //$log.log(buyid);
      //$log.log(ticket);
      //$log.log(percent);
      ordersService.getReturn(buyid, ticket, percent, isCopy)
      .success(function (response) {
        vm.loading=false;
        if (response.error) {
           vm.loading=false;
           messageService.error(response.error.name, response.error.type, response.error.param);
        }
        else {
           vm.loading=false;
           stateService.returns.set({"value":"/orders"});
           stateService.refund.setRefund(response);
           $location.path("/refund");
        }
      }).error(function(response, status) {
		vm.loading=false;
                messageService.error(response.error.name, response.error.type, response.error.param);
		console.log(response);
      });
  };

  
  vm.reBuy = function() {
      if ((vm.entitySelected.status != 'ticket')&&(vm.entitySelected.status != 'success')&&(vm.entitySelected.status != 'hasret')) {
         return;
      }
      if (!vm.entitySelected.buyId) {
         return;
      }
      vm.loading=true;
      var buyid = vm.entitySelected.buyId;
      ordersService.getRebuy(buyid)
      .success(function (response) {
        vm.loading=false;
        if (response.error) {
           vm.loading=false;
           messageService.error(response.error.name, response.error.type, response.error.param);
        }
        else {
           vm.loading=false;
           stateService.returns.set({"value":"/orders"});
           stateService.ticket.setTicket(response);
           stateService.ticket.setBuyId(vm.entitySelected.buyId);
           $location.path("/ticket");
        }
      }).error(function(response, status) {
		vm.loading=false;
                if ((status == -1)||(!responce)) {
                    messageService.error("Немає зв\'язку з сервером", "buy", "");
                } else {
                    messageService.error(response.error.name, response.error.type, response.error.param);
                }
      });
    };
 
    stateService.orders.restore(vm);
    vm.getPage();

};

function ModalController($scope, $uibModal, $uibModalInstance, $log, param) {
        var mvm = this;
        mvm.returnInfo = param.returnInfo;
        mvm.returnPercentSelected = null;
        //mvm.vm = param.entireScope;
  
        mvm.ok = function() {
            $uibModalInstance.close(mvm.returnPercentSelected);
        };

        mvm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

};

function ModalControllerCancel($scope, $uibModal, $uibModalInstance, $log, ordersService, param) {
        var mcvm = this;
        mcvm.cancelInProgress=false;
        mcvm.cancelMessages=[];
        mcvm.entitySelected=param.entitySelected;
        //mvm.vm = param.entireScope;
  
        mcvm.ok = function() {
            mcvm.doCancel('cancel');
        };

        mcvm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        mcvm.doCancel = function (type) {
            if ((type!=='clean')&&(type!=='cancel')) return;
            if ((type=='cancel')&&(mcvm.entitySelected.status !== 'ticket')&&(mcvm.entitySelected.status !== 'success')) {
               return;
            }
            if ((type=='clean')&&(mcvm.entitySelected.status !== 'hung')) {
               return;
            }
            if (!mcvm.entitySelected.buyId) {
               return;
            }
            var buyid = mcvm.entitySelected.buyId;
            mcvm.cancelInProgress=true;
            mcvm.cancelMessages=[];
            ordersService.getCancel(buyid)
            .success(function (response) {
              mcvm.cancelInProgress=false;
              if (response.error) {
                 mcvm.cancelMessages.push({'status':false,'code':response.error.code, 'name':response.error.name, 'param':response.error.param});
              }
              else {
                 mcvm.cancelMessages.push({'status':true,'code':'УСПІШНО', 'name':'Скасування замовлення виконано успішно!', 'param':mcvm.entitySelected.orderNum});
              }
            }).error(function(response, status) {
                      mcvm.cancelInProgress=false;
                      if ((!response)||(response.status === -1)) {
                          mcvm.cancelMessages.push({'status':false,'code':"NO_CONNECTION", 'name':'Немає зв\'язку з сервером', 'param':''});
                          messageService.error("Немає зв\'язку з сервером", "cancel", "");
                      } else {
                          mcvm.cancelMessages.push({'status':false,'code':response.error.code, 'name':response.error.name, 'param':response.error.param});
                      }
            });
            
        };

};


function ModalControllerPay($scope, $uibModal, $uibModalInstance, $log, ordersService, orderService, stateService, $location, param) {
        var mpvm = this;
        mpvm.cancelInProgress=false;
        mpvm.cancelMessages=[];
        mpvm.entity=param.entitySelected;
  
        mpvm.ok = function() {
            mpvm.doPay();
        };

        mpvm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        mpvm.doPay = function () {
            var buyid = mpvm.entity.buyId;
            mpvm.payInProgress=true;
            mpvm.payMessages=[];
            orderService.buy("", [], buyid, 0, function (response) {
                if (response.status === 200) {
                    mpvm.payInProgress = false;
                    stateService.returns.set({"value":"/orders"});
                    stateService.ticket.setTicket(response.data);
                    stateService.ticket.setBuyId(mpvm.entity.buyId);
                    $location.path("/ticket");
                    $uibModalInstance.close();
                } else {
                    mpvm.payInProgress = false;
                    if (responseInit.status === -1) {
                        mpvm.cancelMessages.push({'status':false,'code':"ERROR_CONNECTION", 'name':"Немає зв\'язку", 'param':""});
                    } 
                    else {
                        mpvm.cancelMessages.push({'status':false,'code':response.data.error.code, 'name':response.data.error.name, 'param':response.data.error.param});
                    }
                }
            });
        };
};

