'use strict';

angular.module("Autostation").factory("messageService", messageService);
messageService.$inject = ["$rootScope"];

function messageService($rootScope) {
    var service = {};
    service.error = error;
   
    initService();
    return service;

    function initService() {
        $rootScope.$on('$locationChangeStart', function () {
            clearFlashMessage();
        });

        function clearFlashMessage() {
            var message = $rootScope.message;
            if (message) {
                if (!message.keepAfterLocationChange) {
                    delete $rootScope.message;
                } else {
                    message.keepAfterLocationChange = false;
                }
            }
        }
    }
    function error(message, type, param) {
        $rootScope.message = {
            message: message,
            param: param,
            type: type,
            keepAfterLocationChange: false
        };
    };

    

};
