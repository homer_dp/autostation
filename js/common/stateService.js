'use strict';

angular.module("Autostation").factory("stateService", stateService);
stateService.$inject = ["$localStorage"];

function stateService($localStorage) {

  var states = this;
  states.store = $localStorage;

  states.orders = {};
  states.races = {};
  states.sales = {};
  states.returns = {};
  states.trip = {};
  states.ticket = {};
  states.refund = {};
  states.report = {};
  states.reserve = {};
  states.orders.save = function(controllerObj) {
      states.store.orders_eDate = controllerObj.eDate;
      states.store.orders_bDate = controllerObj.bDate;
      states.store.orders_datepickers = controllerObj.datepickers;
      //states.orders.gridSelected = controllerObj.gridSelected;
      //states.orders.entitySelected = controllerObj.entitySelected;
      //states.orders.styleSelected = controllerObj.styleSelected;
      //states.orders.gridOptions = controllerObj.gridOptions;
  }
  states.orders.restore = function(controllerObj) {
      if (states.store.orders_eDate) {
        if ((typeof states.store.orders_eDate) =="string") { controllerObj.eDate = new Date(states.store.orders_eDate) }
        else { controllerObj.eDate = states.store.orders_eDate; }
      }
      else {
          controllerObj.eDate = new Date();
      };
      if (states.store.orders_bDate) {
        if ((typeof states.store.orders_bDate) =="string") { controllerObj.bDate = new Date(states.store.orders_bDate) }
        else { controllerObj.bDate = states.store.orders_bDate; }
      }
      else {
          controllerObj.bDate = new Date();
          controllerObj.bDate.setDate(controllerObj.bDate.getDate()-1);
      };
      if (states.store.orders_datepickers) controllerObj.datepickers = states.store.orders_datepickers;
      //if (states.orders.gridSelected) {
      //   controllerObj.gridSelected = states.orders.gridSelected;
      //   controllerObj.entitySelected = states.orders.entitySelected;
      //   controllerObj.styleSelected = states.orders.styleSelected;
      //}
      //if (states.orders.gridOptions) controllerObj.gridOptions = states.orders.gridOptions;
  }


  states.races.save = function(controllerObj) {
      states.store.races_eDate = controllerObj.eDate;
      states.store.races_bDate = controllerObj.bDate;
      states.store.races_datepickers = controllerObj.datepickers;
  }
  states.races.restore = function(controllerObj) {
      if (states.store.races_eDate) {
        if ((typeof states.store.races_eDate) =="string") { controllerObj.eDate = new Date(states.store.races_eDate) }
        else { controllerObj.eDate = states.store.races_eDate; }
      }
      else {
          controllerObj.eDate = new Date();
      };
      if (states.store.races_bDate) {
        if ((typeof states.store.races_bDate) =="string") { controllerObj.bDate = new Date(states.store.races_bDate) }
        else { controllerObj.bDate = states.store.races_bDate; }
      }
      else {
          controllerObj.bDate = new Date();
      };
      if (states.store.races_datepickers) controllerObj.datepickers = states.store.races_datepickers;
  }


  states.sales.save = function(controllerObj) {
      states.store.sales_eDate = controllerObj.eDate;
      states.store.sales_bDate = controllerObj.bDate;
      states.store.sales_datepickers = controllerObj.datepickers;
  }
  states.sales.restore = function(controllerObj) {
      if (states.store.sales_eDate) {
        if ((typeof states.store.sales_eDate) =="string") { controllerObj.eDate = new Date(states.store.sales_eDate) }
        else { controllerObj.eDate = states.store.sales_eDate; }
      }
      else {
          controllerObj.eDate = new Date();
      };
      if (states.store.sales_bDate) {
        if ((typeof states.store.sales_bDate) =="string") { controllerObj.bDate = new Date(states.store.sales_bDate) }
        else { controllerObj.bDate = states.store.sales_bDate; }
      }
      else {
          controllerObj.bDate = new Date();
      };
      if (states.store.sales_datepickers) controllerObj.datepickers = states.store.sales_datepickers;
  }


  states.report.save = function(controllerObj) {
      states.store.report_eDate = controllerObj.eDate;
      states.store.report_bDate = controllerObj.bDate;
      states.store.report_datepickers = controllerObj.datepickers;
      states.store.report_id = controllerObj.reportId;
  }
  states.report.restore = function(controllerObj) {
      if (states.store.report_eDate) {
        if ((typeof states.store.report_eDate) =="string") { controllerObj.eDate = new Date(states.store.report_eDate) }
        else { controllerObj.eDate = states.store.report_eDate; }
      }
      else {
          controllerObj.eDate = new Date();
      };
      if (states.store.report_bDate) {
        if ((typeof states.store.report_bDate) =="string") { controllerObj.bDate = new Date(states.store.report_bDate) }
        else { controllerObj.bDate = states.store.report_bDate; }
      }
      else {
          controllerObj.bDate = new Date();
      };
      if (states.store.report_datepickers) controllerObj.datepickers = states.store.report_datepickers;
      if (states.store.report_id) controllerObj.reportId = states.store.report_id;

  }


  states.returns.set = function(locationStr) {
      states.returns.locationStr = locationStr;
  }
  states.returns.get = function() {
      var locationStr = {}; 
      if (states.returns.locationStr) locationStr = states.returns.locationStr;
      return locationStr;
  }

  states.trip.getTripId = function () {
      return states.store.tripInfo_tripId;
  }
  states.trip.setTripId = function (tripId) {
      states.store.tripInfo_tripId = tripId;
  }
  states.trip.getTripData = function () {
      return states.store.tripInfo_tripData;
  }
  states.trip.setTripData = function (dataObject) {
      states.store.tripInfo_tripData = dataObject;
  }
  states.trip.removeTripInfo = function () {
      states.store.tripInfo_tripData = undefined;
  }


  states.ticket.getTicket = function() {
      return states.store.ticket_value;
  }
  states.ticket.setTicket = function (dataObject) {
      return states.store.ticket_value=dataObject;
  }
  states.ticket.getBuyId = function() {
     return states.store.buy_id;
  }
  states.ticket.setBuyId = function(buyId) {
     return states.store.buy_id=buyId;
  }


  states.refund.getRefund = function () {
      return states.store.refund_value;
  }
  states.refund.setRefund = function (dataObject) {
      return states.store.refund_value=dataObject;
  }

  states.reserve.getReserve = function () {
      return states.store.reserve_value;
  }
  states.reserve.setReserve = function (dataObject) {
      return states.store.reserve_value=dataObject;
  }
  states.reserve.getBuyId = function() {
     return states.store.reserve_buy_id;
  }
  states.reserve.setBuyId = function(buyId) {
     return states.store.reserve_buy_id=buyId;
  }

 
  states.clean = function () {
      states.store.$reset();
  } 
 

  return states;

};
