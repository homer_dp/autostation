/* js/common/mainurl.js content:
============================================================================

'use strict';
angular.module("Autostation").service("urlService", urlService);

function urlService() {
  return {
      getUrl: function() {
         return "http://172.16.0.1/test2/cgi-bin/gtmapp/";  // localhost & dp-asu
         // return "cgi-bin-test/gtmapp/";                  // uak-gate
         // return "/cgi-bin/gtmapp/";                      // zubustik
      }
  };
};

============================================================================
*/


'use strict';
angular.module("Autostation").service("constantsService", constants);
constants.$inject = ["urlService"];

function constants(urlService) {
    return {
        ticketsUrl: {
            cities: urlService.getUrl() + "wapi/cities",
            //cities: URL() + "wapi/cities",
            routes: urlService.getUrl() + "wapi/trips",
            stations: urlService.getUrl() + "wapi/route"
        },
        accountnUrl: {
            login: urlService.getUrl() + "wapi/login"
        },
        orderUrl: {
            trip: urlService.getUrl() + "wapi/trip",
            buyinint: urlService.getUrl() + "wapi/buyinit",
            buy: urlService.getUrl() + "wapi/buy",
            exportPdf: urlService.getUrl() + "wapi/exportPDF"
        },
        ordersUrl: {
            orders: urlService.getUrl() + "wapi/orders",
            cancel: urlService.getUrl() + "wapi/cancel",
            retinfo: urlService.getUrl() + "wapi/retinfo",
            return_: urlService.getUrl() + "wapi/return"
        },
        racesUrl: {
            races: urlService.getUrl() + "wapi/races",
            race: urlService.getUrl() + "wapi/race",
            vedomXls: urlService.getUrl() + "wapi/vedomXLS",
            racectl: urlService.getUrl() + "wapi/racectl"
        },
        salesUrl: {
            sales: urlService.getUrl() + "wapi/sales"
        },
        reportUrl: {
            reportXls: urlService.getUrl() + "wapi/reportXLS"
        },
        errorType: {
            login: "login",
            cities: "cities",
            trips: "trips",
            trip: "trip",
            route: "route",
            buyinit: "buyinit",
            buy: "buy",
            cancel: "cancel",
            orders: "orders",
            retinfo: "retinfo",
            return_: "return",
            races: "races",
            race: "race",
            racectl: "racectl",
            sales: "sales"
        }

    }
};
