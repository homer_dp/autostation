'use strict';

angular.module('Autostation').factory('stateService', function(){

  var states = {};

  states.orders = {};
  states.returns = {};
  states.trip = trip();
  states.ticket = ticket();
  states.refund = refund();
  states.orders.save = function(controllerObj) {
      states.orders.eDate = controllerObj.eDate;
      states.orders.bDate = controllerObj.bDate;
      states.orders.datepickers = controllerObj.datepickers;
      //states.orders.gridSelected = controllerObj.gridSelected;
      //states.orders.entitySelected = controllerObj.entitySelected;
      //states.orders.styleSelected = controllerObj.styleSelected;
      //states.orders.gridOptions = controllerObj.gridOptions;
  }
  states.orders.restore = function(controllerObj) {
      if (states.orders.eDate) {
        controllerObj.eDate = states.orders.eDate;
      }
      else {
          controllerObj.eDate = new Date();
      };
      if (states.orders.bDate) {
          controllerObj.bDate = states.orders.bDate;
      }
      else {
          controllerObj.bDate = new Date();
          controllerObj.bDate.setDate(controllerObj.bDate.getDate()-1);
      };
      if (states.orders.datepickers) controllerObj.datepickers = states.orders.datepickers;
      //if (states.orders.gridSelected) {
      //   controllerObj.gridSelected = states.orders.gridSelected;
      //   controllerObj.entitySelected = states.orders.entitySelected;
      //   controllerObj.styleSelected = states.orders.styleSelected;
      //}
      //if (states.orders.gridOptions) controllerObj.gridOptions = states.orders.gridOptions;
  }
  states.returns.set = function(locationStr) {
      states.returns.locationStr = locationStr;
  }
  states.returns.get = function() {
      var locationStr = {}; 
      if (states.returns.locationStr) locationStr = states.returns.locationStr;
      return locationStr;
  }

  function trip() {
        var tripInfo = {};
        return {
            getTripId: function () {
                return tripInfo.tripId;
            },
            setTripId: function (tripId) {
                tripInfo.tripId = tripId;

            },
            getTripData: function () {
                return tripInfo.tripData;
            },
            setTripData: function (dataObject) {
                tripInfo.tripData = dataObject;
            },
            removeTripInfo: function () {
                tripInfo.tripData = undefined;
            }
        };
  }

  function ticket() {
      var ticketItem = {};
      return {
          getTicket: function () {
              return ticketItem;
          },
          setTicket: function (item) {
              ticketItem = item;
          }
      };
  }

  function refund() {
      var refundItem = {};
      return {
          getRefund: function () {
              return refundItem;
          },
          setRefund: function (item) {
              refundItem = item;
          }
      };
  }

  return states;

});