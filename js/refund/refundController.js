﻿'use strict';

angular.module("Refund").controller("RefundController", RefundController);
RefundController.$inject = ["$scope", "stateService",  "$location"];

function RefundController($scope, stateService,  $location) {
    var vm = this;
    vm.returnCopyStyle={};

    (function () {
        if (stateService.refund.getRefund() !== undefined)
            vm.refund = stateService.refund.getRefund();
        if (vm.refund.isCopy==1) {
          vm.returnCopyStyle={'background-size':'contain,contain', 'background-image':'url(pictures/copy.png)'};
        }
        else {
          vm.returnCopyStyle={};
        }
        //console.log(vm.refund);
     })();
   
    vm.print = function() {
        window.print();
    }

    vm.backFromRefund = function() {
        var locationStr = stateService.returns.get();
        if (locationStr.value) {
          $location.path(locationStr.value); 
        }
        else {
          $location.path("/orders");
        }
    }




};