"use strict";

angular.module("Tickets", []);
angular.module("Account", []);
angular.module("Order", []);
angular.module("Orders", []);
angular.module("Sales", []);
angular.module("Races", []);
angular.module("Ticket", []);
angular.module("Refund", []);
angular.module("Reserve", []);
angular.module("Report", []);
angular.module("Autostation", ["Tickets", "Account", "Order", "Orders", "Sales", "Races", "Ticket", "Refund", "Reserve", "Report", "ngRoute", "angucomplete-alt", "ui.bootstrap", "ngLocale", "ngCookies", "ui.mask", "ngStorage"])
    .config([
        "$routeProvider", function($routeProvider) {
            $routeProvider
                .when("/tickets", {
                    controller: "TicketsController as tickets",
                    templateUrl: "views/tickets/Tickets.html"
                }).when("/login", {
                    controller: "LoginController as loginCtrl",
                    templateUrl: "views/account/Login.html"
                }).when("/orders", {
                    controller: "OrdersController as ordersCtrl",
                    templateUrl: "views/orders/Orders.html"
                }).when("/races", {
                    controller: "RacesController as racesCtrl",
                    templateUrl: "views/races/Races.html"
                }).when("/sales", {
                    controller: "SalesController as salesCtrl",
                    templateUrl: "views/sales/Sales.html"
                }).when("/order", {
                    controller: "OrderController as orderCtrl",
                    templateUrl: "views/order/Order.html"
                }).when("/refund", {
                    controller: "RefundController as refundCtrl",
                    templateUrl: "views/refund/refund.html"
                }).when("/reserve", {
                    controller: "ReserveController as reserveCtrl",
                    templateUrl: "views/reserve/reserve.html"
                }).when("/report", {
                    controller: "ReportController as reportCtrl",
                    templateUrl: "views/report/report.html"
                }).when("/ticket", {
                    controller: "TicketController as ticketCtrl",
                    templateUrl: "views/ticket/ticket.html"
                })
                .otherwise({ redirectTo: "/tickets" });
        }
    ]).run([
        "$rootScope", "$location", "$cookies", "$window", "$http", function($rootScope, $location, $cookies, $window, $http) {
            //ToDo Implement removing cookie after close browser
            //if ($rootScope.isLogin == undefined) {
            //    $window.onbeforeunload = function () {
            //        $cookies.remove("userAutostation");
            //        $rootScope.isLogin = true;
            //    };
            //}
            $rootScope.userGlobal = $cookies.getObject('userAutostation') || {};
            if ($rootScope.userGlobal.token) {
                $http.defaults.headers.common.session = $rootScope.userGlobal.token;
                $http.defaults.headers.common.userlogin = $rootScope.userGlobal.login;

            }
            $rootScope.$on('$locationChangeStart', function(event, next, current) {
                var loggedIn = $rootScope.userGlobal.token;
                if (!loggedIn) {
                    $location.path('/login');
                }
            });
        }
    ])
    .controller("ModalInstanceCtrl", ModalInstanceCtrl)
    .controller("GlobalController", GlobalController);
    ModalInstanceCtrl.$inject = ["$scope", "$uibModalInstance", "stations"];
GlobalController.$inject = ["$scope","stateService"];


function ModalInstanceCtrl($scope, $uibModalInstance, stations) {

    $scope.stations = stations;
    $scope.exit = function () {
        $uibModalInstance.dismiss('cancel');
    };
};

function GlobalController($scope, stateService) {
    $scope.removeTripData = function() {
        stateService.trip.removeTripInfo();
        
    };
};