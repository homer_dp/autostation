﻿'use strict';

angular.module("Ticket").controller("TicketController", TicketController);
TicketController.$inject = ["$rootScope", "$scope", "stateService", "constantsService", "$location"];

function TicketController($rootScope, $scope, stateService, constantsService, $location) {
    var vm = this;
    vm.loading = false;
    vm.ticketCopyStyle={};
    (function () {
        if (stateService.ticket.getTicket() !== undefined) {
            vm.ticket = stateService.ticket.getTicket()
            vm.buyId = stateService.ticket.getBuyId()
        };
        if (vm.ticket.isCopy==1) {
          vm.ticketCopyStyle={'background-size':'contain,contain', 'background-image':'url(pictures/copy.png)'};
        }
        else {
          vm.ticketCopyStyle={};
        }
        //console.log(vm.ticket);
    })();
   
    vm.print = function() {
        window.print();
    }

    vm.loadPdf = function() {
       return constantsService.orderUrl.exportPdf+'?token='+$rootScope.userGlobal.token+'&buyid='+vm.buyId;
    }

    vm.backFromTicket = function() {
        var locationStr = stateService.returns.get();
        if (locationStr.value) {
          $location.path(locationStr.value); 
        }
        else {
          $location.path("/tickets");
        }
        stateService.trip.removeTripInfo();
    }




};